<?php

namespace App\Services\Shop\Interfaces;

use App\Services\Shop\Interfaces\Entities\BuyerInterface;

interface BuyerServiceInterface
{
    /**
     * Создает в возвращает покупателя
     * @return BuyerInterface
     */
    public function add(): BuyerInterface;
}
