<?php

namespace App\Services\Shop\Transformers;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Transformers\Transformer;

class CategoryShowTransformer extends Transformer
{
    /**
     * @param CategoryInterface $category
     *
     * @return array|mixed
     */
    public function transform($category)
    {
        return [
            'id' => $category->getId(),
            'name' => $category->getName(),
            'slug' => $category->getSlug(),
            'updated_at' => date('d.m.Y H:i', strtotime($category->getUpdatedAt())),
        ];
    }
}
