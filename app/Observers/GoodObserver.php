<?php

namespace App\Observers;

use App\Models\Good;
use Cache;

class GoodObserver
{
    /**
     * Handle the good "created" event.
     *
     * @param Good $good
     *
     * @return void
     */
    public function created(Good $good)
    {
        $this->clearCache();
    }

    /**
     * Clear cache
     */
    private function clearCache()
    {
        Cache::tags('goods')
            ->flush();
    }

    /**
     * Handle the good "updated" event.
     *
     * @param Good $good
     *
     * @return void
     */
    public function updated(Good $good)
    {
        $this->clearCache();
    }

    /**
     * Handle the good "deleted" event.
     *
     * @param Good $good
     *
     * @return void
     */
    public function deleted(Good $good)
    {
        $this->clearCache();
    }

    /**
     * Handle the good "restored" event.
     *
     * @param Good $good
     *
     * @return void
     */
    public function restored(Good $good)
    {
        $this->clearCache();
    }

    /**
     * Handle the good "force deleted" event.
     *
     * @param Good $good
     *
     * @return void
     */
    public function forceDeleted(Good $good)
    {
        $this->clearCache();
    }
}
