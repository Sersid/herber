<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')
    ->group(function () {
        Route::get('/shop/new-orders', 'Api\Shop\OrderController@last');
        Route::get('/shop/orders', 'Api\Shop\OrderController@paginate');
        Route::get('/shop/goods', 'Api\Shop\GoodController@list');
        Route::post('/shop/good', 'Api\Shop\GoodController@store');
        Route::post('/shop/image', 'Api\Shop\ImageController@upload');
        Route::post('/shop/category', 'Api\Shop\CategoryController@store');
        Route::where(['id' => '[0-9]+'])
            ->group(
                function () {
                    Route::get('/shop/order/{id}', 'Api\Shop\OrderController@show');
                    Route::get('/shop/good/{id}', 'Api\Shop\GoodController@show');
                    Route::patch('/shop/good/{id}', 'Api\Shop\GoodController@update');
                    Route::delete('/shop/good/{id}', 'Api\Shop\GoodController@remove');
                    Route::post('/shop/good/copy/{id}', 'Api\Shop\GoodController@copy');
                    Route::delete('/shop/image/{id}', 'Api\Shop\ImageController@remove');
                    Route::get('/shop/category/{id}', 'Api\Shop\CategoryController@show');
                    Route::patch('/shop/category/{id}', 'Api\Shop\CategoryController@update');
                    Route::delete('/shop/category/{id}', 'Api\Shop\CategoryController@remove');
                }
            );
        Route::get('/shop/categories/dropdown', 'Api\Shop\CategoryController@dropdown');
        Route::get('/shop/categories', 'Api\Shop\CategoryController@paginate');
        Route::get('/shop/images', 'Api\Shop\ImageController@list');
    });
