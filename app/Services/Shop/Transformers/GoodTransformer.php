<?php

namespace App\Services\Shop\Transformers;

use App\Helpers\PriceHelper;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Transformers\Transformer;

class GoodTransformer extends Transformer
{
    /**
     * @param GoodInterface $good
     *
     * @return array|mixed
     */
    public function transform($good)
    {
        return [
            'id' => $good->getId(),
            'name' => $good->getName(),
            'url' => $good->getUrl(),
            'category' => $good->getCategory()->getName(),
            'price' => PriceHelper::format($good->getPrice()),
            'old_price' => PriceHelper::format($good->getOldPrice()),
        ];
    }
}
