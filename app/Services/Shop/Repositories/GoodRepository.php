<?php

namespace App\Services\Shop\Repositories;

use App\Models\Good as Model;
use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Repositories\GoodRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class GoodRepository extends Repository implements GoodRepositoryInterface
{
    /**
     * @inheritDoc
     * @return GoodInterface|Model|object|null
     */
    public function getEntityForShow(int $id): ?GoodInterface
    {
        if (empty($id)) {
            return null;
        }
        return $this->query()
            ->select(['id', 'category_id', 'name', 'slug', 'price', 'old_price', 'short_text', 'long_text', 'props'])
            ->with([
                'images' => function (HasMany $query) {
                    $query->select(['id', 'good_id', 'md', 'lg']);
                },
                'category' => function (HasOne $query) {
                    $query->select(['id', 'name', 'slug']);
                },
            ])
            ->where(['id' => $id])
            ->first();
    }

    /**
     * @inheritDoc
     * @return GoodInterface|Model|object|null
     */
    public function getEntityForBasket(int $id): ?GoodInterface
    {
        return $this->query()
            ->select(['id', 'price'])
            ->where('id', $id)
            ->first();
    }

    /**
     * @inheritDoc
     * @return GoodInterface|Model|object|null
     */
    public function getEntityForEdit(int $id): ?GoodInterface
    {
        return $this->query()
            ->select([
                'id',
                'name',
                'slug',
                'category_id',
                'price',
                'old_price',
                'short_text',
                'long_text',
                'props',
                'created_at',
                'updated_at',
            ])
            ->with([
                'category' => function (HasOne $query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->where('id', $id)
            ->first();
    }

    /**
     * @inheritDoc
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return $this->query()
            ->select(['id', 'category_id', 'slug', 'name', 'price', 'old_price'])
            ->with([
                'thumbnail' => function (HasOne $query) {
                    $query->select(['id', 'good_id', 'md']);
                },
                'category' => function (HasOne $query) {
                    $query->select(['id', 'name', 'slug']);
                },
            ])
            ->paginate(25);
    }

    /**
     * @inheritDoc
     */
    public function getPaginateByCategory(CategoryInterface $category): LengthAwarePaginator
    {
        return $this->query()
            ->select(['id', 'category_id', 'slug', 'name', 'price', 'old_price'])
            ->where('category_id', $category->getId())
            ->with([
                'category' => function (HasOne $query) {
                    $query->select(['id', 'slug']);
                },
                'thumbnail' => function (HasOne $query) {
                    $query->select(['id', 'good_id', 'md']);
                },
            ])
            ->paginate(25);
    }

    /**
     * @inheritDoc
     */
    public function getPaginatorForAdmin(int $perPage): LengthAwarePaginator
    {
        return $this->query()
            ->select(['id', 'category_id', 'slug', 'name', 'price', 'old_price'])
            ->with([
                'category' => function (HasOne $query) {
                    $query->select(['id', 'slug', 'name']);
                },
            ])
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return Model::class;
    }
}
