<?php

namespace App\Services\Shop\Interfaces;

use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\ImageInterface;
use Illuminate\Http\UploadedFile;

/**
 * Действия с изрбражениями товаров
 * @package App\Services\Shop
 */
interface ImageServiceInterface
{
    /**
     * Создает информацию о картинке
     *
     * @param UploadedFile $image
     * @param int|null     $good_id
     *
     * @return ImageInterface
     */
    public function upload(UploadedFile $image, ?int $good_id): ImageInterface;

    /**
     * Присваивает товар картинкам, у которых он не указан
     * @param GoodInterface $good
     *
     * @return mixed
     */
    public function updateGoodForEmpty(GoodInterface $good);

    /**
     * Удаляет картинку
     *
     * @param ImageInterface $image
     */
    public function remove(ImageInterface $image);

    /**
     * Копирование изображений из одного товара в другой
     * @param GoodInterface $fromGood
     * @param GoodInterface $toGood
     *
     * @return mixed
     */
    public function copy(GoodInterface $fromGood, GoodInterface $toGood);
}
