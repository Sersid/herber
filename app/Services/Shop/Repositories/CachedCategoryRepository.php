<?php

namespace App\Services\Shop\Repositories;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Repositories\CategoryRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CachedCategoryRepository extends CachedRepository implements CategoryRepositoryInterface
{
    private CategoryRepositoryInterface $repository;

    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function getPaginatorForAdmin(int $perPage): LengthAwarePaginator
    {
        return $this->repository->getPaginatorForAdmin($perPage);
    }

    /**
     * @inheritDoc
     */
    public function getForMenu(): array
    {
        return $this->getCache('for-menu', fn() => $this->repository->getForMenu());
    }

    /**
     * @inheritDoc
     */
    public function getForFilter(): array
    {
        return $this->getCache('for-filter', fn() => $this->repository->getForFilter());
    }

    /**
     * @inheritDoc
     */
    public function getForDropdown(): Collection
    {
        return $this->getCache('for-dropdown', fn() => $this->repository->getForDropdown());
    }

    /**
     * @inheritDoc
     */
    public function getEntityBySlug(string $slug): ?CategoryInterface
    {
        return $this->getCache('slug-' . $slug, fn() => $this->repository->getEntityBySlug($slug));
    }

    /**
     * @inheritDoc
     */
    public function getEntityForEdit(int $id): ?CategoryInterface
    {
        return $this->repository->getEntityForEdit($id);
    }

    /**
     * @inheritDoc
     */
    protected function tag(): string
    {
        return 'categories';
    }

    /**
     * @inheritDoc
     */
    protected function durations(): int
    {
        return 3600;
    }
}
