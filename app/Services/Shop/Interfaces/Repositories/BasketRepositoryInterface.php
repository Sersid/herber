<?php

namespace App\Services\Shop\Interfaces\Repositories;

use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;

interface BasketRepositoryInterface
{
    /**
     * Позиция в корзине по покупателю и товару
     *
     * @param GoodInterface  $good
     * @param BuyerInterface $buyer
     *
     * @return BasketItemInterface|null
     */
    public function getBasketItem(BuyerInterface $buyer, GoodInterface $good): ?BasketItemInterface;

    /**
     * Список всех позиций в корзине по покупателю для вывода
     *
     * @param BuyerInterface $buyer
     *
     * @return BasketItemInterface[]
     */
    public function getAllByBuyer(BuyerInterface $buyer);

    /**
     * Сумма всех позиций в корзине по покупателю
     *
     * @param BuyerInterface $buyer
     *
     * @return float
     */
    public function getSumByBuyer(BuyerInterface $buyer): float;
}
