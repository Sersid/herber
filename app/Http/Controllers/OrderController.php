<?php

namespace App\Http\Controllers;

use App\Helpers\PriceHelper;
use App\Http\Requests\OrderRequest;
use App\Services\Shop\Interfaces\BasketServiceInterface;
use App\Services\Shop\Interfaces\OrderServiceInterface;
use App\Services\Shop\Interfaces\Repositories\BasketRepositoryInterface;
use App\Services\Shop\Interfaces\Repositories\BuyerRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * @package App\Http\Controllers
 */
final class OrderController extends Controller
{
    private BasketServiceInterface $basketService;
    private OrderServiceInterface $orderService;
    private BuyerRepositoryInterface $buyerRepository;
    private BasketRepositoryInterface $basketRepository;

    public function __construct(
        OrderServiceInterface $orderService,
        BasketServiceInterface $basketService,
        BuyerRepositoryInterface $buyerRepository,
        BasketRepositoryInterface $basketRepository
    ) {
        $this->basketService = $basketService;
        $this->orderService = $orderService;
        $this->buyerRepository = $buyerRepository;
        $this->basketRepository = $basketRepository;
    }

    /**
     * Просмотр корзины и заполнение формы
     * @return View
     */
    public function cart()
    {
        return view('shop.cart');
    }

    /**
     * Процесс оформления заказа
     *
     * @param OrderRequest $request
     *
     * @return JsonResponse
     */
    public function checkout(OrderRequest $request): JsonResponse
    {
        $buyer = $this->buyerRepository->getCurrent();
        if (empty($buyer)) {
            abort(500, 'Покупатель не найден');
        }
        $sum = $this->basketRepository->getSumByBuyer($buyer);
        if (empty($sum)) {
            abort(500, 'Корзина пуста');
        }
        $order = $this->orderService->checkout(
            $sum,
            $request->get('name'),
            $request->get('phone'),
            $request->get('email'),
            $request->get('info')
        );
        $this->basketService->updateOrder($buyer, $order);
        return response()->json(['id' => $order->getId(), 'sum' => PriceHelper::format($order->getSum())], 201);
    }
}
