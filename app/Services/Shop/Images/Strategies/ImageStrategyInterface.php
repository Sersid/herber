<?php

namespace App\Services\Shop\Images\Strategies;

interface ImageStrategyInterface
{
    /**
     * ������� ��� ����������� ���������� �����������
     * @return string
     */
    public function getSuffix(): string;

    /**
     * ������ �����������
     * @return int|null
     */
    public function getWidth(): ?int;

    /**
     * ������ �����������
     * @return int|null
     */
    public function getHeight(): ?int;
}
