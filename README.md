# Интернет-магазин на Laravel
### Установка проекта
Подразумевается, что система Linux и уже установлены: ``git, make, docker, docker-compose`` 
1) Клонирование проекта
    ```
    git clone git@bitbucket.org:Sersid/herber.git
    ```
    При необходимости обновить модули
    ```
    git submodule update
    ```
2) Запустить установку
    ```
    make install
   ```
