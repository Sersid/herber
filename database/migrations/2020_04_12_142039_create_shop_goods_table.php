<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopGoodsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('shop_goods',
            function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->softDeletes();
                $table->string('name');
                $table->string('slug');
                $table->unsignedBigInteger('category_id')
                    ->index();
                $table->unsignedDecimal('price')
                    ->index();
                $table->unsignedDecimal('old_price')
                    ->nullable();
                $table->text('short_text')
                    ->nullable();
                $table->text('long_text')
                    ->nullable();
                $table->json('props')
                    ->nullable();
            });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_goods');
    }
}
