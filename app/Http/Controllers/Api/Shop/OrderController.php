<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\ApiController;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use App\Services\Shop\Interfaces\OrderServiceInterface as Service;
use App\Services\Shop\Interfaces\Repositories\OrderRepositoryInterface as Repository;
use App\Services\Shop\Transformers\OrderShowTransformer;
use App\Services\Shop\Transformers\OrderTransformer as Transformer;
use Illuminate\Http\JsonResponse;

final class OrderController extends ApiController
{
    protected Service $service;
    protected Repository $repository;

    public function __construct(Repository $repository, Service $service, Transformer $transformer)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Последние заказы
     * @return JsonResponse
     */
    public function last()
    {
        $orders = $this->repository->getLast(10);
        return $this->respondWithTransformer($orders);
    }

    /**
     * Заказы с постраничной навигацией
     * @return JsonResponse
     */
    public function paginate()
    {
        $paginator = $this->repository->getPaginator(50);
        return $this->respondWithPaginator($paginator);
    }

    /**
     * Детальная информация о заказе
     *
     * @param int                  $id
     * @param OrderShowTransformer $transformer
     *
     * @return JsonResponse
     */
    public function show(int $id, OrderShowTransformer $transformer)
    {
        $order = $this->repository->getEntity($id);
        if (empty($order)) {
            $this->respondNotFound('Order not found');
        }
        if ($order->isNew()) {
            $this->service->updateStatus($order, OrderInterface::STATUS_IN_WORK);
        }
        $this->transformer = $transformer;
        return $this->respondWithTransformer($order);
    }
}
