<?php

namespace App\Services\Shop\Interfaces\Entities;

interface BuyerInterface
{
    /**
     * Id покупателя
     * @return int
     */
    public function getId(): int;
}
