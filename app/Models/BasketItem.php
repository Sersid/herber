<?php

namespace App\Models;

use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @package App\Services\Shop\Entities
 * @property int                 id
 * @property float               price
 * @property int                 quantity
 * @property int                 buyer_id
 * @property int                 good_id
 * @property int                 order_id
 * @property GoodInterface|null  good
 * @property BuyerInterface|null buyer
 */
class BasketItem extends BaseModel implements BasketItemInterface
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'shop_baskets';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['buyer_id', 'good_id', 'price', 'quantity', 'order_id'];

    /**
     * @return HasOne
     */
    public function good()
    {
        return $this->hasOne(Good::class, 'id', 'good_id');
    }

    /**
     * @return HasOne
     */
    public function buyer()
    {
        return $this->hasOne(Buyer::class, 'id', 'buyer_id');
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getSum(): float
    {
        return $this->getPrice() * $this->getQuantity();
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): int
    {
        return (int)$this->quantity;
    }

    /**
     * @inheritDoc
     */
    public function getGood(): ?GoodInterface
    {
        return $this->good;
    }

    /**
     * @inheritDoc
     */
    public function getBuyer(): BuyerInterface
    {
        return $this->buyer;
    }
}
