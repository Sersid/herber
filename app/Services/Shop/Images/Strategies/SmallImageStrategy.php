<?php

namespace App\Services\Shop\Images\Strategies;

class SmallImageStrategy implements ImageStrategyInterface
{
    /**
     * @inheritDoc
     */
    function getSuffix(): string
    {
        return 'sm';
    }

    /**
     * @inheritDoc
     */
    function getWidth(): ?int
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    function getHeight(): ?int
    {
        return 100;
    }
}
