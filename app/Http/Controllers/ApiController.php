<?php

namespace App\Http\Controllers;

use App\Transformers\Transformer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

abstract class ApiController extends Controller
{
    protected Transformer $transformer;

    /**
     * Respond with data after applying transformer.
     *
     * @param       $data
     * @param int   $statusCode
     * @param array $headers
     *
     * @return JsonResponse
     */
    protected function respondWithTransformer($data, int $statusCode = 200, array $headers = [])
    {
        if ($data instanceof Collection) {
            $data = $this->transformer->collection($data);
        } else {
            $data = $this->transformer->item($data);
        }
        return $this->respond($data, $statusCode, $headers);
    }

    /**
     * Return generic json response with the given data.
     *
     * @param       $data
     * @param int   $statusCode
     * @param array $headers
     *
     * @return JsonResponse
     */
    protected function respond($data, int $statusCode = 200, array $headers = [])
    {
        return response()->json($data, $statusCode, $headers);
    }

    /**
     * Respond with pagination.
     *
     * @param LengthAwarePaginator $paginator
     * @param int                  $statusCode
     * @param array                $headers
     *
     * @return JsonResponse
     */
    protected function respondWithPaginator(LengthAwarePaginator $paginator, int $statusCode = 200, array $headers = [])
    {
        $data = $this->transformer->paginate($paginator);
        return $this->respond($data, $statusCode, $headers);
    }

    /**
     * Respond with success.
     * @return JsonResponse
     */
    protected function respondSuccess()
    {
        return $this->respond(null);
    }

    /**
     * Respond with created.
     *
     * @param $data
     *
     * @return JsonResponse
     */
    protected function respondCreated($data)
    {
        return $this->respond($data, 201);
    }

    /**
     * Respond with no content.
     * @return JsonResponse
     */
    protected function respondNoContent()
    {
        return $this->respond(null, 204);
    }

    /**
     * Respond with unauthorized.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    protected function respondUnauthorized(string $message = 'Unauthorized')
    {
        return $this->respondError($message, 401);
    }

    /**
     * Respond with error.
     *
     * @param $message
     * @param $statusCode
     *
     * @return JsonResponse
     */
    protected function respondError(string $message, int $statusCode)
    {
        return $this->respond(
            [
                'errors' => [
                    'message' => $message,
                    'status_code' => $statusCode,
                ],
            ],
            $statusCode
        );
    }

    /**
     * Respond with forbidden.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    protected function respondForbidden(string $message = 'Forbidden')
    {
        return $this->respondError($message, 403);
    }

    /**
     * Respond with not found.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    protected function respondNotFound(string $message = 'Not Found')
    {
        return $this->respondError($message, 404);
    }

    /**
     * Respond with failed login.
     * @return JsonResponse
     */
    protected function respondFailedLogin()
    {
        return $this->respond(
            [
                'errors' => [
                    'email or password' => 'is invalid',
                ],
            ],
            422
        );
    }

    /**
     * Respond with internal error.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    protected function respondInternalError(string $message = 'Internal Error')
    {
        return $this->respondError($message, 500);
    }
}
