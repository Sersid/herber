<!doctype html>
<html class="wide wow-animation" lang="en">
<head>
    <title>Herber</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('images/favicon.ico') }}" rel="image/x-icon">
    <!-- Stylesheets-->
    <link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@700&display=swap" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
<div class="preloader">
    <div class="preloader-body">
        <div class="cssload-container">
            <span></span><span></span><span></span><span></span>
        </div>
    </div>
</div>
<div class="page">
    <!-- Page Header-->
    <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap rd-navbar-modern-wrap">
            <nav class="rd-navbar rd-navbar-modern" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="70px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
                <div class="rd-navbar-main-outer">
                    <div class="rd-navbar-main">
                        <!-- RD Navbar Panel-->
                        <div class="rd-navbar-panel">
                            <!-- RD Navbar Toggle-->
                            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span>
                            </button>
                            <!-- RD Navbar Brand-->
                            <div class="rd-navbar-brand">
                                <a class="brand" href="/"><img src="/images/logo-default-154x47.png" alt="" width="154" height="47"/></a>
                            </div>
                        </div>
                        <div class="rd-navbar-main-element">
                            <div class="rd-navbar-nav-wrap">
                                <!-- RD Navbar Basket-->
                                <basket></basket>
                                <!-- RD Navbar Search-->
                                <div class="rd-navbar-search">
                                    <button class="rd-navbar-search-toggle" data-rd-navbar-toggle=".rd-navbar-search">
                                        <span></span>
                                    </button>
                                    <form class="rd-search" action="search-results.html" data-search-live="rd-search-results-live" method="GET">
                                        <div class="form-wrap">
                                            <label class="form-label" for="rd-navbar-search-form-input">Search...</label>
                                            <input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off">
                                            <div class="rd-search-results-live" id="rd-search-results-live"></div>
                                            <button class="rd-search-form-submit fl-bigmug-line-search74" type="submit"></button>
                                        </div>
                                    </form>
                                </div>
                                @widget('mainMenu')
                            </div>
                            <div class="rd-navbar-project-hamburger" data-multitoggle=".rd-navbar-main" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate>
                                <div class="project-hamburger">
                                    <span class="project-hamburger-arrow-top"></span><span class="project-hamburger-arrow-center"></span><span class="project-hamburger-arrow-bottom"></span>
                                </div>
                                <div class="project-hamburger-2">
                                    <span class="project-hamburger-arrow"></span><span class="project-hamburger-arrow"></span><span class="project-hamburger-arrow"></span>
                                </div>
                                <div class="project-close"><span></span><span></span></div>
                            </div>
                        </div>
                        <div class="rd-navbar-project rd-navbar-modern-project">
                            <div class="rd-navbar-project-modern-header">
                                <h4 class="rd-navbar-project-modern-title">Связаться с нами</h4>
                                <div class="rd-navbar-project-hamburger" data-multitoggle=".rd-navbar-main" data-multitoggle-blur=".rd-navbar-wrap" data-multitoggle-isolate>
                                    <div class="project-close"><span></span><span></span></div>
                                </div>
                            </div>
                            <div class="rd-navbar-project-content rd-navbar-modern-project-content">
                                <div>
                                    <p>Мы всегда готовы предоставить вам свежие фермерские продукты для вашего дома или
                                        офиса. Свяжитесь с нами, чтобы узнать все варианты сотрудничества.</p>
                                    <div class="row row-10 gutters-10">
                                        <div class="col-12">
                                            <img src="/images/home-sidebar-394x255.jpg" alt="" width="394" height="255"/>
                                        </div>
                                    </div>
                                    <ul class="rd-navbar-modern-contacts">
                                        <li>
                                            <div class="unit unit-spacing-sm">
                                                <div class="unit-left"><span class="icon fa fa-phone"></span></div>
                                                <div class="unit-body"><a class="link-phone" href="tel:#">+7
                                                        323-913-4687</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="unit unit-spacing-sm">
                                                <div class="unit-left"><span class="icon fa fa-location-arrow"></span>
                                                </div>
                                                <div class="unit-body"><a class="link-location" href="#">4730
                                                        Краснодарский Край, село «Краснознаменское», ул. Ленина 25</a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="unit unit-spacing-sm">
                                                <div class="unit-left"><span class="icon fa fa-envelope"></span></div>
                                                <div class="unit-body"><a class="link-email" href="mailto:#">mail@demolink.org</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    @yield('content')
    <!-- Page Footer-->
    <footer class="section footer-variant-2 footer-modern context-dark section-top-image section-top-image-dark">
        <div class="footer-variant-2-content">
            <div class="container">
                <div class="row row-40 justify-content-between">
                    <div class="col-sm-6 col-lg-4 col-xl-3">
                        <div class="oh-desktop">
                            <div class="wow slideInRight" data-wow-delay="0s">
                                <div class="footer-brand">
                                    <a href="/"><img src="/images/logo-inverse-154x42.png" alt="" width="154" height="42"/></a>
                                </div>
                                <p>Хербер - это органическая ферма, расположенная в Краснодарском Крае. Мы предлагаем
                                    нашим клиентам здоровую пищу и продукты питания.</p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-4">
                        <div class="oh-desktop">
                            <div class="inset-top-18 wow slideInDown" data-wow-delay="0s">
                                <ul class="footer-contacts d-inline-block d-md-block">
                                    <li>
                                        <div class="unit unit-spacing-xs">
                                            <div class="unit-left"><span class="icon fa fa-phone"></span></div>
                                            <div class="unit-body"><a class="link-phone" href="tel:#">+7
                                                    323-913-4688</a></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="unit unit-spacing-xs">
                                            <div class="unit-left"><span class="icon fa fa-clock-o"></span></div>
                                            <div class="unit-body">
                                                <p>Пн-Вс: 07:00 - 19:00</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="unit unit-spacing-xs">
                                            <div class="unit-left"><span class="icon fa fa-location-arrow"></span></div>
                                            <div class="unit-body"><a class="link-location" href="#">4730 Краснодарский
                                                    Край, село «Краснознаменское», ул. Ленина 25</a></div>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-3">
                        <div class="oh-desktop">
                            <div class="inset-top-18 wow slideInLeft" data-wow-delay="0s">
                                <h5>Gallery</h5>
                                <div class="row row-10 gutters-10">
                                    <div class="col-6 col-sm-3 col-lg-6">
                                        <!-- Thumbnail Classic-->
                                        <article class="thumbnail thumbnail-mary">
                                            <div class="thumbnail-mary-figure">
                                                <img src="/images/gallery-image-1-129x120.jpg" alt="" width="129" height="120"/>
                                            </div>
                                            <div class="thumbnail-mary-caption">
                                                <a class="icon fl-bigmug-line-zoom60" href="/images/gallery-original-7-800x1200.jpg" data-lightgallery="item"><img src="/images/gallery-image-1-129x120.jpg" alt="" width="129" height="120"/></a>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-6 col-sm-3 col-lg-6">
                                        <!-- Thumbnail Classic-->
                                        <article class="thumbnail thumbnail-mary">
                                            <div class="thumbnail-mary-figure">
                                                <img src="/images/gallery-image-2-129x120.jpg" alt="" width="129" height="120"/>
                                            </div>
                                            <div class="thumbnail-mary-caption">
                                                <a class="icon fl-bigmug-line-zoom60" href="/images/gallery-original-8-1200x800.jpg" data-lightgallery="item"><img src="/images/gallery-image-2-129x120.jpg" alt="" width="129" height="120"/></a>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-6 col-sm-3 col-lg-6">
                                        <!-- Thumbnail Classic-->
                                        <article class="thumbnail thumbnail-mary">
                                            <div class="thumbnail-mary-figure">
                                                <img src="/images/gallery-image-3-129x120.jpg" alt="" width="129" height="120"/>
                                            </div>
                                            <div class="thumbnail-mary-caption">
                                                <a class="icon fl-bigmug-line-zoom60" href="/images/gallery-original-9-800x1200.jpg" data-lightgallery="item"><img src="/images/gallery-image-3-129x120.jpg" alt="" width="129" height="120"/></a>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-6 col-sm-3 col-lg-6">
                                        <!-- Thumbnail Classic-->
                                        <article class="thumbnail thumbnail-mary">
                                            <div class="thumbnail-mary-figure">
                                                <img src="/images/gallery-image-4-129x120.jpg" alt="" width="129" height="120"/>
                                            </div>
                                            <div class="thumbnail-mary-caption">
                                                <a class="icon fl-bigmug-line-zoom60" href="/images/gallery-original-10-1200x800.jpg" data-lightgallery="item"><img src="/images/gallery-image-4-129x120.jpg" alt="" width="129" height="120"/></a>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-variant-2-bottom-panel">
            <div class="container">
                <!-- Rights-->
                <div class="group-sm group-sm-justify">
                    <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span> <span>Herber</span>.
                        Все права защищены
                    </p>
                    <p class="rights"><a href="/privacy-policy">Политика обработки персональных данных</a></p>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
</div>
<!-- Javascript-->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('js/core.min.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
