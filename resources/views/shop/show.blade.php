@extends('template')

@section('content')
    @include('widgets.breadcrumbs', [
        'items' => [
            [
                'name' => 'Каталог продукции',
                'url' => url('shop')
            ],
            [
                'name' => $good->getCategory()->getName(),
                'url' => $good->getCategory()->getUrl()
            ],
        ],
        'title' => $good->getName(),
    ])
    <!-- Single Product-->
    <section class="section section-md section-first bg-default">
        <div class="container">
            <div class="row row-50">
                <div class="col-lg-6">
                    @if ($good->hasImages())
                    <div class="slick-product">
                        <!-- Slick Carousel-->
                        <div class="slick-slider carousel-parent" data-swipe="true" data-items="1" data-child="#child-carousel" data-for="#child-carousel">
                            @foreach($good->getImages() as $image)
                            <div class="item">
                                <div class="slick-product-figure"><img src="{{ $image->getLarge() }}" alt="" width="530" height="480"/>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="slick-slider child-carousel" id="child-carousel" data-for=".carousel-parent" data-arrows="true" data-items="3" data-sm-items="3" data-md-items="3" data-lg-items="3" data-xl-items="3" data-slide-to-scroll="1" data-md-vertical="true">
                            @foreach($good->getImages() as $image)
                            <div class="item">
                                <div class="slick-product-figure"><img src="{{ $image->getMedium() }}" alt="" width="169" height="152"/>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-lg-6">
                    <div class="single-product">
                        <h3>{{ $good->getName() }}</h3>
                        <div class="group-md group-middle">
                            <div class="single-product-price">{{ $good->getFormattedPrice() }}</div>
                        </div>
                        @if($good->hasShortText())
                            <p>{{ $good->getShortText() }}</p>
                        @endif
                        <div class="divider divider-30"></div>
                        @if($good->hasProps())
                            <ul class="list list-description d-inline-block d-md-block">
                                @foreach($good->getProps() as $prop)
                                    <li><span>{{ $prop['name'] }}:</span><span>{{ $prop['value'] }}</span></li>
                                @endforeach
                            </ul>
                        @endif
                        <add-to-basket-detail :id="{{ $good->getId() }}" />
                        <div class="group-sm group-middle">
                            <div class="product-stepper">
                                <input class="form-input" type="number" data-zeros="true" value="1" min="1" max="1000">
                            </div>
                            <div><a class="button button-primary button-pipaluk" href="#">Добавить в корзину</a></div>
                        </div>
                        <div class="divider divider-40"></div>
                        <div class="group-md group-middle"><span class="social-title">Поделиться</span>
                            <div>
                                <ul class="list-inline list-inline-sm social-list">
                                    <li><a class="icon fa fa-facebook" target="_blank" href="https://www.facebook.com/sharer.php?u={{ urlencode($good->getUrl()) }}"></a></li>
                                    <li><a class="icon fa fa-twitter" target="_blank" href="http://twitter.com/share?url={{ urlencode($good->getUrl()) }}"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Bootstrap tabs-->
            <div class="tabs-custom tabs-horizontal tabs-corporate" id="tabs-5">
                <!-- Nav tabs-->
                <ul class="nav nav-tabs">
                    @if($good->hasLongText())
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-5-1" data-toggle="tab">Дополнительная информация</a></li>
                    @endif
                    <li class="nav-item" role="presentation"><a class="nav-link {{ !$good->hasLongText() ? 'active' : '' }}" href="#tabs-5-2" data-toggle="tab">Доставка и оплата</a></li>
                </ul>
                <!-- Tab panes-->
                <div class="tab-content">
                    @if($good->hasLongText())
                    <div class="tab-pane fade show active" id="tabs-5-1">
                        {{ $good->getLongText() }}
                    </div>
                    @endif
                    <div class="tab-pane fade {{ !$good->hasLongText() ? 'show active' : '' }}" id="tabs-5-2">
                        <p>Вы делаете заказ за сутки. В ночь продукты расформировываются по хозяйствам и каждое хозяйство получает свой заказ.</p>
                        <p>У каждого хозяйства есть сутки для его подготовки. Вечером или ночью все свозиться в Москву для фасовки по заказам.</p>
                        <p>В ночь происходит фасовка продуктов по заказам. Все скомплектованные заказы подписываются и ночуют в холодильниках в Москве. Основной закон окончания фасовки, не должно остаться продуктов, не укомплектованных по заказам.</p>
                        <p>Начиная с 8 утра, водители собирают заказы, и начинается развозка по адресам.</p>
                        <p>Таким образом, мы гарантированно привозим свежие продукты прямо из хозяйства.</p>
                        <p>Минимальная сумма заказ в нашем магазине - 1 900 руб. </p>
                        <p>Доставка продуктов с фермы производится ЕЖЕДНЕВНО в удобное для Вас время! </p>
                        <p>Для доставки на следующий день, заказ необходимо сделать до 12:00!</p>
                        <p>Заказы сделанные после 12:00 доставляются через день!</p>
                        <p>Подробности по телефонам: +7 (495) 797-06-12, +7 (903) 797-06-12</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if ($good->hasRelatedGoods())
    <!-- Single Product-->
    <section class="section section-md section-last bg-default">
        <div class="container">
            <h4>Сопутствующие товары</h4>
            <div class="row row-40 justify-content-center">
                <div class="col-sm-6 col-md-5 col-lg-3">
                    <!-- Product-->
                    <article class="product">
                        <div class="product-figure"><img src="images/product-1-270x280.png" alt="" width="270" height="280"/>
                            <div class="product-button"><a class="button button-md button-white button-ujarak" href="cart-page.html">Add to cart</a></div>
                        </div>
                        <h5 class="product-title"><a href="single-product.html">Peaches</a></h5>
                        <div class="product-price-wrap">
                            <div class="product-price">$550.00</div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                    <!-- Product-->
                    <article class="product">
                        <div class="product-figure"><img src="images/product-2-270x280.png" alt="" width="270" height="280"/>
                            <div class="product-button"><a class="button button-md button-white button-ujarak" href="cart-page.html">Add to cart</a></div>
                        </div>
                        <h5 class="product-title"><a href="single-product.html">Blueberries</a></h5>
                        <div class="product-price-wrap">
                            <div class="product-price">$650.00</div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                    <!-- Product-->
                    <article class="product">
                        <div class="product-figure"><img src="images/product-3-270x280.png" alt="" width="270" height="280"/>
                            <div class="product-button"><a class="button button-md button-white button-ujarak" href="cart-page.html">Add to cart</a></div>
                        </div>
                        <h5 class="product-title"><a href="single-product.html">Apples</a></h5>
                        <div class="product-price-wrap">
                            <div class="product-price">$350.00</div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                    <!-- Product-->
                    <article class="product">
                        <div class="product-figure"><img src="images/product-4-270x280.png" alt="" width="270" height="280"/>
                            <div class="product-button"><a class="button button-md button-white button-ujarak" href="cart-page.html">Add to cart</a></div>
                        </div>
                        <h5 class="product-title"><a href="single-product.html">Kiwis</a></h5>
                        <div class="product-price-wrap">
                            <div class="product-price">$99.00</div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection
