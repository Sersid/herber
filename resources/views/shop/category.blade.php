@extends('template')

@section('content')
    @include('widgets.breadcrumbs', [
        'items' => [
            [
                'name' => 'Каталог продукции',
                'url' => url('shop')
            ]
        ],
        'title' => $category->getName(),
    ])
    <!-- Shop-->
    <section class="section section-xl bg-default">
        <div class="container">
            <div class="row row-90 justify-content-center">
                <div class="col-xl-12">
                    @if($paginate->isNotEmpty())
                        <div class="product-top-panel group-lg">
                            <div class="product-top-panel-title">Показаны {{ $paginate->firstItem() }}–{{ $paginate->lastItem() }} из {{ $paginate->total() }}</div>
                            <div class="product-top-panel-select">
                                <!--Select 2-->
                                <select class="form-input select-filter" data-minimum-results-for-search="Infinity" data-constraints="@Required">
                                    <option value="1">Сортировать по цене</option>
                                    <option value="2">От меньшего к большему</option>
                                    <option value="3">От большего к меньшему</option>
                                </select>
                            </div>
                        </div>
                        <div class="row row-lg row-40">
                            @foreach($paginate->items() as $good)
                                <div class="col-sm-6 col-md-3">
                                    @include('widgets.product', ['good' => $good])
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h4>Товары не найдены</h4>
                    @endif
                    <div class="pagination-wrap">
                    {{ $paginate->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Footer-->
@endsection
