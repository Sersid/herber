<?php

namespace App\Services\Shop\Transformers;

use App\Services\Shop\Interfaces\Entities\ImageInterface;
use App\Transformers\Transformer;

class ImageTransformer extends Transformer
{
    /**
     * @param ImageInterface $image
     *
     * @return array|mixed
     */
    public function transform($image)
    {
        return [
            'id' => $image->getId(),
            'name' => $image->getName(),
            'sm' => $image->getSmall(),
            'md' => $image->getMedium(),
            'lg' => $image->getLarge(),
        ];
    }
}
