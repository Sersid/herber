<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CategoryRequest;
use App\Services\Shop\Interfaces\CategoryServiceInterface as Service;
use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Repositories\CategoryRepositoryInterface as Respository;
use App\Services\Shop\Transformers\CategoryDropdownTransformer;
use App\Services\Shop\Transformers\CategoryPaginateTransformer;
use App\Services\Shop\Transformers\CategoryShowTransformer;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class CategoryController extends ApiController
{
    protected Respository $repository;
    protected Service $service;

    public function __construct(Respository $repository, Service $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Разделы с постраничной навигацией
     *
     * @param CategoryPaginateTransformer $transformer
     *
     * @return JsonResponse
     */
    public function paginate(CategoryPaginateTransformer $transformer)
    {
        $paginator = $this->repository->getPaginatorForAdmin(25);
        $this->transformer = $transformer;
        return $this->respondWithPaginator($paginator);
    }

    /**
     * Список всех категорий для выпадающего списка
     *
     * @param CategoryDropdownTransformer $transformer
     *
     * @return JsonResponse
     */
    public function dropdown(CategoryDropdownTransformer $transformer)
    {
        $categories = $this->repository->getForDropdown();
        $this->transformer = $transformer;
        return $this->respondWithTransformer($categories);
    }

    /**
     * Детальная информация о заказе
     *
     * @param int                     $id
     * @param CategoryShowTransformer $transformer
     *
     * @return JsonResponse
     */
    public function show(int $id, CategoryShowTransformer $transformer)
    {
        try {
            $this->transformer = $transformer;
            return $this->respondWithTransformer($this->getEntity($id));
        } catch (NotFoundHttpException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * @param int $id
     *
     * @return CategoryInterface
     * @throws NotFoundHttpException
     */
    private function getEntity(int $id): CategoryInterface
    {
        $category = $this->repository->getEntityForEdit($id);
        if (empty($category)) {
            throw new NotFoundHttpException('Category not found');
        }
        return $category;
    }

    /**
     * Создание категории
     *
     * @param CategoryRequest $request
     *
     * @return JsonResponse
     */
    public function store(CategoryRequest $request): JsonResponse
    {
        $good = $this->service->create($this->getDataFromRequest($request));
        return $this->respondCreated(['item' => ['id' => $good->getId()]]);
    }

    /**
     * @param CategoryRequest $request
     *
     * @return array
     */
    private function getDataFromRequest(CategoryRequest $request): array
    {
        return [
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
        ];
    }

    /**
     * Обновляет категорию
     *
     * @param int             $id
     * @param CategoryRequest $request
     *
     * @return JsonResponse
     */
    public function update(int $id, CategoryRequest $request): JsonResponse
    {
        try {
            $this->service->update($this->getEntity($id), $this->getDataFromRequest($request));
            return $this->respondSuccess();
        } catch (NotFoundHttpException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * Удаление категории
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function remove(int $id): JsonResponse
    {
        try {
            $this->service->remove($this->getEntity($id));
            return $this->respondSuccess();
        } catch (NotFoundHttpException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }
}
