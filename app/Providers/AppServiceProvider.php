<?php

namespace App\Providers;

use App\Models;
use App\Observers;
use App\Services\Shop;
use App\Services\Shop\Interfaces;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public array $bindings = [
        Interfaces\Repositories\GoodRepositoryInterface::class => Shop\Repositories\CachedGoodRepository::class,
        Interfaces\Repositories\CategoryRepositoryInterface::class => Shop\Repositories\CachedCategoryRepository::class,
        Interfaces\Repositories\BasketRepositoryInterface::class => Shop\Repositories\BasketRepository::class,
        Interfaces\Repositories\BuyerRepositoryInterface::class => Shop\Repositories\BuyerRepository::class,
        Interfaces\Repositories\OrderRepositoryInterface::class => Shop\Repositories\OrderRepository::class,
        Interfaces\Repositories\ImageRepositoryInterface::class => Shop\Repositories\ImageRepository::class,
        Interfaces\GoodServiceInterface::class => Shop\GoodService::class,
        Interfaces\OrderServiceInterface::class => Shop\OrderService::class,
        Interfaces\BasketServiceInterface::class => Shop\BasketService::class,
        Interfaces\BuyerServiceInterface::class => Shop\BuyerService::class,
        Interfaces\ImageServiceInterface::class => Shop\ImageService::class,
        Interfaces\CategoryServiceInterface::class => Shop\CategoryService::class,
    ];

    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        $this->app->when(Shop\Repositories\CachedGoodRepository::class)
            ->needs(Interfaces\Repositories\GoodRepositoryInterface::class)
            ->give(Shop\Repositories\GoodRepository::class);

        $this->app->when(Shop\Repositories\CachedCategoryRepository::class)
            ->needs(Interfaces\Repositories\CategoryRepositoryInterface::class)
            ->give(Shop\Repositories\CategoryRepository::class);
    }

    /**
     * Bootstrap any application services.
     * @return void
     */
    public function boot()
    {
        Models\Good::observe(Observers\GoodObserver::class);
        Models\Category::observe(Observers\CategoryObserver::class);
    }
}
