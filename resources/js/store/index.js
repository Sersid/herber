import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        basket: {
            items: [],
            count: 0,
            total_sum: 0,
            formatted_total_sum: ''
        },
    },
    getters: {
        BASKET: state => {
            return state.basket;
        }
    },
    mutations: {
        SET_BASKET: (state, payload) => {
            state.basket = payload;
        }
    }
});
