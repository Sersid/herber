<?php

namespace App\Models;

use App\Services\Shop\Interfaces\Entities\ImageInterface;
use Illuminate\Support\Facades\File;

/**
 * @property int id
 * @property int good_id
 * @property string name
 * @property string sm
 * @property string md
 * @property string lg
 */
class Image extends BaseModel implements ImageInterface
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'shop_images';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['good_id', 'name', 'sm', 'md', 'lg'];

    /**
     * @inheritDoc
     */
    public function getSmall(): string
    {
        return $this->sm;
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getMedium(): string
    {
        return $this->md;
    }

    /**
     * @inheritDoc
     */
    public function getLarge(): string
    {
        return $this->lg;
    }

    /**
     * @inheritDoc
     */
    public function getDir(): string
    {
        return File::dirname($this->getSmall());
    }

    /**
     * @inheritDoc
     */
    public function getExt(): string
    {
        return last(explode('.', $this->getSmall()));
    }

    /**
     * @inheritDoc
     */
    public function getDirPath(): string
    {
        return public_path($this->getDir());
    }

    /**
     * @inheritDoc
     */
    public function getSmallPath(): string
    {
        return public_path($this->getSmall());
    }

    /**
     * @inheritDoc
     */
    public function getMediumPath(): string
    {
        return public_path($this->getMedium());
    }

    /**
     * @inheritDoc
     */
    public function getLargePath(): string
    {
        return public_path($this->getLarge());
    }
}
