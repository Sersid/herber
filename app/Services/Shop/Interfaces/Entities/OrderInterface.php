<?php

namespace App\Services\Shop\Interfaces\Entities;

interface OrderInterface
{
    // Новый заказ
    const STATUS_NEW = 100;
    // Заказ в работе
    const STATUS_IN_WORK = 200;
    // Заказ отменен
    const STATUS_CANCEL = 900;
    // Заказ выполнен
    const STATUS_DONE = 1000;

    /**
     * ID
     * @return int
     */
    public function getId(): int;

    /**
     * Номер заказа
     * @return string
     */
    public function getNumber(): string;

    /**
     * Дата создания заказа
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Код статуса заказа
     * @return int
     */
    public function getStatus(): int;

    /**
     * Название статуса заказа
     * @return string
     */
    public function getStatusName(): string;

    /**
     * Является новым заказом
     * @return bool
     */
    public function isNew(): bool;

    /**
     * Содержимое корзины
     * @return BasketItemInterface[]
     */
    public function getBasket(): array;

    /**
     * Сумма заказа
     * @return float
     */
    public function getSum(): float;

    /**
     * Имя покупателя
     * @return string
     */
    public function getName(): string;

    /**
     * Телефон покупателя
     * @return string
     */
    public function getPhone(): string;

    /**
     * E-mail покупателя
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * Дополнительная информация о заказе
     * @return string|null
     */
    public function getInfo(): ?string;
}
