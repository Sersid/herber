<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('shop', 'ShopController@index');
Route::get('shop/basket', 'BasketController@index');
Route::where(['id' => '[0-9]+'])
    ->group(
        function () {
            Route::get('shop/basket/{id}', 'BasketController@test');
            Route::post('shop/basket/{id}', 'BasketController@add');
            Route::patch('shop/basket/{id}', 'BasketController@update');
            Route::delete('shop/basket/{id}', 'BasketController@remove');
        }
    );
Route::get('shop/cart', 'OrderController@cart');
Route::post('shop/checkout', 'OrderController@checkout');
Route::get('shop/{slug}', 'ShopController@category');
Route::get('shop/{category}/{id}-{slug}', 'ShopController@show')
    ->where('id', '[0-9]+');
Route::middleware('auth')
    ->get('/admin',
        function () {
            $token = Auth::user()->api_token;
            return view('admin', compact('token'));
        });
Auth::routes(['register' => false, 'password.request' => false]);
