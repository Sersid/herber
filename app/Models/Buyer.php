<?php

namespace App\Models;

use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @package App\Services\Shop\Entities
 * @property int          id
 * @property string       ip
 * @property BasketItem[] basket
 */
class Buyer extends BaseModel implements BuyerInterface
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'shop_buyers';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['ip'];

    /**
     * @return HasMany
     */
    public function basket()
    {
        return $this->hasMany(BasketItem::class, 'buyer_id', 'id');
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }
}
