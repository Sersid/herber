<?php

namespace App\Services\Shop\Interfaces;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;

interface CategoryServiceInterface
{
    /**
     * Создание категории
     *
     * @param array $data
     *
     * @return CategoryInterface
     */
    public function create(array $data): CategoryInterface;

    /**
     * Обновление категории
     *
     * @param CategoryInterface $category
     * @param array             $data
     */
    public function update(CategoryInterface $category, array $data);

    /**
     * Удаление категории
     *
     * @param CategoryInterface $category
     */
    public function remove(CategoryInterface $category);
}
