@extends('template')
@section('content')
    <!-- Swiper-->
    <section class="section swiper-container swiper-slider swiper-slider-modern" data-loop="true" data-autoplay="5000" data-simulate-touch="true" data-nav="true" data-slide-effect="fade">
        <div class="swiper-wrapper text-left">
            <div class="swiper-slide context-dark" data-slide-bg="images/slider-1-1920x729.jpg">
                <div class="swiper-slide-caption">
                    <div class="container">
                        <div class="row justify-content-center justify-content-xxl-start">
                            <div class="col-md-10 col-xxl-6">
                                <div class="slider-modern-box">
                                    <h1 class="slider-modern-title"><span data-caption-animate="slideInDown" data-caption-delay="0">Фермерские овощи</span></h1>
                                    <p data-caption-animate="fadeInRight" data-caption-delay="400">Мы обеспечиваем местных жителей и гостей нашего города качественными фермерскими овощами. Попробуйте и Вы!</p>
                                    <div class="oh button-wrap"><a class="button button-primary button-ujarak" href="/shop" data-caption-animate="slideInLeft" data-caption-delay="400">Посмотреть продукцию</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide context-dark" data-slide-bg="images/slider-2-1920x729.jpg">
                <div class="swiper-slide-caption">
                    <div class="container">
                        <div class="row justify-content-center justify-content-xxl-start">
                            <div class="col-md-10 col-xxl-6">
                                <div class="slider-modern-box">
                                    <h1 class="slider-modern-title"><span data-caption-animate="slideInLeft" data-caption-delay="0">Свежие фрукты</span></h1>
                                    <p data-caption-animate="fadeInRight" data-caption-delay="400">Мы контролируем процесс ведения сельского хозяйства, чтобы поставлять лучшие фрукты конечному потребителю</p>
                                    <div class="oh button-wrap"><a class="button button-primary button-ujarak" href="/shop" data-caption-animate="slideInLeft" data-caption-delay="400">Посмотреть продукцию</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" data-slide-bg="images/slider-3-1920x729.jpg">
                <div class="swiper-slide-caption">
                    <div class="container">
                        <div class="row justify-content-center justify-content-xxl-start">
                            <div class="col-md-10 col-xxl-6">
                                <div class="slider-modern-box">
                                    <h1 class="slider-modern-title"><span data-caption-animate="slideInDown" data-caption-delay="0">Домашняя консервация</span></h1>
                                    <p data-caption-animate="fadeInRight" data-caption-delay="400">Как ведущая органическая ферма, мы придерживаемся экологически чистой политики выращивания овощей, из которых производится наша консервация</p>
                                    <div class="oh button-wrap"><a class="button button-primary button-ujarak" href="/shop" data-caption-animate="slideInUp" data-caption-delay="400">Посмотреть продукцию</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Swiper Navigation-->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        <!-- Swiper Pagination-->
        <div class="swiper-pagination swiper-pagination-style-2"></div>
    </section>

    <!-- Icons Ruby-->
    <section class="section section-md bg-default section-top-image">
        <div class="container">
            <div class="row row-30 justify-content-center">
                <div class="col-sm-6 col-lg-4 wow fadeInRight" data-wow-delay="0s">
                    <article class="box-icon-ruby">
                        <div class="unit box-icon-ruby-body flex-column flex-md-row text-md-left flex-lg-column align-items-center text-lg-center flex-xl-row text-xl-left">
                            <div class="unit-left">
                                <div class="box-icon-ruby-icon linearicons-leaf"></div>
                            </div>
                            <div class="unit-body">
                                <h4 class="box-icon-ruby-title"><a href="#">Натуральные продукты</a></h4>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4 wow fadeInRight" data-wow-delay=".1s">
                    <article class="box-icon-ruby">
                        <div class="unit box-icon-ruby-body flex-column flex-md-row text-md-left flex-lg-column align-items-center text-lg-center flex-xl-row text-xl-left">
                            <div class="unit-left">
                                <div class="box-icon-ruby-icon linearicons-shovel"></div>
                            </div>
                            <div class="unit-body">
                                <h4 class="box-icon-ruby-title"><a href="#">Фермерское хозяйство</a></h4>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4 wow fadeInRight" data-wow-delay=".2s">
                    <article class="box-icon-ruby">
                        <div class="unit box-icon-ruby-body flex-column flex-md-row text-md-left flex-lg-column align-items-center text-lg-center flex-xl-row text-xl-left">
                            <div class="unit-left">
                                <div class="box-icon-ruby-icon linearicons-sun"></div>
                            </div>
                            <div class="unit-body">
                                <h4 class="box-icon-ruby-title"><a href="#">Польза природы</a></h4>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
@endsection
