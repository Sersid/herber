<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static $this create(array $data)
 */
abstract class BaseModel extends Model
{
}
