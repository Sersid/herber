import quantity from './quantity';
export default {
    mixins: [quantity],
    props: {
        item: {
            type: Object,
            required: true
        }
    },
    created() {
        this.debouncedSave = _.debounce(this.save, 1000)
    },
    mounted() {
        this.quantity = this.item.quantity;
    },
    methods: {
        save() {
            if (this.item.quantity !== this.quantity) {
                if (this.quantity === 0) {
                    // удаление из корзины
                    axios.delete('/shop/basket/' + this.item.id)
                        .then(response => {
                            this.$store.commit('SET_BASKET', response.data);
                        });
                } else {
                    // изменение кол-ва
                    axios.patch('/shop/basket/' + this.item.id, {quantity: this.quantity})
                        .then(response => {
                            this.$store.commit('SET_BASKET', response.data);
                        });
                }
            }
        },
    },
    watch: {
        quantity(val) {
            this.quantity = val < 0 ? 0 : val;
            this.debouncedSave();
        },
        item(val) {
            this.quantity = val.quantity;
        }
    }
}
