<?php

namespace App\Services\Shop\Transformers;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Transformers\Transformer;

class CategoryDropdownTransformer extends Transformer
{
    /**
     * @param CategoryInterface $category
     *
     * @return array|mixed
     */
    public function transform($category)
    {
        return [
            'value' => $category->getId(),
            'text' => $category->getName(),
        ];
    }
}
