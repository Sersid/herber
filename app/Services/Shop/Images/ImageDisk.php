<?php

namespace App\Services\Shop\Images;

use App\Services\Shop\Images\Strategies\ImageStrategyInterface;

class ImageDisk implements ImageStrategyInterface
{
    /** @var string ������������ ����� ��� ���������� */
    private string $name;
    /** @var string ���������� ����� */
    private string $ext;

    private ImageStrategyInterface $strategy;

    public function __construct(string $name, string $ext)
    {
        $this->name = $name;
        $this->ext = $ext;
    }

    /**
     * ��������� ���������
     *
     * @param ImageStrategyInterface $strategy
     */
    public function setStrategy(ImageStrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * ������� ��� ����������� ���������� �����������
     * @return string
     */
    public function getSuffix(): string
    {
        return $this->strategy->getSuffix();
    }

    /**
     * ������ �����������
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->strategy->getWidth();
    }

    /**
     * ������ �����������
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->strategy->getHeight();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getExt(): string
    {
        return $this->ext;
    }

    /**
     * ���������� ������������ public
     * @return string
     */
    public function getDir(): string
    {
        return '/upload/' . substr($this->getName(), 0, 3);
    }

    /**
     * ���������� ���� � ����������
     * @return string
     */
    public function getDirPath(): string
    {
        return public_path($this->getDir());
    }

    /**
     * ���� � ������������
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getDir() . '/' . $this->getName() . '_' . $this->getSuffix() . '.' . $this->getExt();
    }

    /**
     * ���������� ���� � �����������
     * @return string
     */
    public function getPath(): string
    {
        return public_path($this->getUrl());
    }
}
