export default {
    data() {
        return {
            url: '',
            removeUrl: '',
            showLoader: true,
            showConfirm: false,
            removeIndex: 0,
            currentPage: 1,
            lastPage: 1,
            total: 100,
            perPage: 10,
            items: []
        }
    },
    created() {
        this.fetch();
    },
    watch: {
        currentPage() {
            this.fetch();
        }
    },
    computed: {
        removeItem() {
            return typeof this.items[this.removeIndex] !== 'undefined' ? this.items[this.removeIndex] : {id: 0, name: ''};
        }
    },
    methods: {
        fetch() {
            axios.get(this.url, {params: {page: this.currentPage}}).then(response => {
                this.currentPage = response.data.current_page;
                this.perPage = response.data.per_page;
                this.total = response.data.total;
                this.lastPage = response.data.last_page;
                this.items = response.data.items;
                this.showLoader = false;
            });
        },
        confirmRemove(index) {
            this.removeIndex = index;
            this.showConfirm = true;
        },
        remove() {
            axios.delete(this.removeUrl + '/' + this.removeItem.id)
                .then(() => {
                    this.showModal = false;
                    this.items.splice(this.removeIndex, 1);
                })
                .catch(e => {
                    alert(e.response.data.errors);
                });
        }
    }
}

