<?php

namespace App\Services\Shop\Images\Strategies;

class MediumImageStrategy implements ImageStrategyInterface
{
    /**
     * @inheritDoc
     */
    function getSuffix(): string
    {
        return 'md';
    }

    /**
     * @inheritDoc
     */
    function getWidth(): ?int
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    function getHeight(): ?int
    {
        return 300;
    }
}
