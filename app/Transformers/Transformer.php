<?php

namespace App\Transformers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

abstract class Transformer
{
    /**
     * Resource name of the json object.
     */
    protected string $resourceName = 'item';

    /**
     * Transform a collection of items.
     *
     * @param Collection $data
     *
     * @return array
     */
    public function collection(Collection $data): array
    {
        return [
            Str::plural($this->resourceName) => $data->map([$this, 'transform']),
        ];
    }

    /**
     * Transform a single item.
     *
     * @param $data
     *
     * @return array
     */
    public function item($data): array
    {
        return [
            $this->resourceName => $this->transform($data)
        ];
    }

    /**
     * Apply the transformation.
     *
     * @param $data
     *
     * @return mixed
     */
    public abstract function transform($data);

    /**
     * Transform a paginated item.
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    public function paginate(LengthAwarePaginator $paginator): array
    {
        return array_merge(
            $this->collection(collect($paginator->items())),
            [
                'total' => $paginator->total(),
                'current_page' => $paginator->currentPage(),
                'per_page' => $paginator->perPage(),
                'last_page' => $paginator->lastPage(),
            ]
        );
    }
}
