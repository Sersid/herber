<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopBasketsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('shop_baskets',
            function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->unsignedBigInteger('buyer_id')
                    ->index();
                $table->unsignedBigInteger('good_id')
                    ->index();
                $table->unsignedBigInteger('order_id')
                    ->index()
                    ->nullable();
                $table->bigInteger('quantity');
                $table->unsignedDecimal('price')
                    ->index();
            });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_baskets');
    }
}
