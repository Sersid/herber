<?php

namespace App\Services\Shop;

use App\Models\Buyer;
use App\Services\Shop\Interfaces\BuyerServiceInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;

class BuyerService implements BuyerServiceInterface
{
    public function add(): BuyerInterface
    {
        $buyer = Buyer::create(['ip' => \Request::ip()]);
        session(['buyer_id' => $buyer->getId()]);
        return $buyer;
    }
}
