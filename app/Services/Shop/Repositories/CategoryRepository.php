<?php

namespace App\Services\Shop\Repositories;

use App\Models\Category as Model;
use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Repositories\CategoryRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * @package App\Repositories\Shop
 */
class CategoryRepository extends Repository implements CategoryRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getForMenu(): array
    {
        return $this->query()
            ->select(['name', 'slug'])
            ->orderBy('sort')
            ->get()
            ->all();
    }

    /**
     * @inheritDoc
     * @return CategoryInterface|Model|object|null
     */
    public function getEntityBySlug(string $slug): ?CategoryInterface
    {
        if (empty($slug)) {
            return null;
        }
        return $this->query()
            ->select(['id', 'name', 'slug'])
            ->where('slug', $slug)
            ->first();
    }

    /**
     * @inheritDoc
     */
    public function getForFilter(): array
    {
        return $this->query()
            ->select(['id', 'name', 'slug'])
            ->withCount('goods')
            ->having('goods_count', '>', 0)
            ->get()
            ->all();
    }

    /**
     * @inheritDoc
     */
    public function getForDropdown(): Collection
    {
        return $this->query()
            ->select(['id', 'name'])
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function getPaginatorForAdmin(int $perPage): LengthAwarePaginator
    {
        return $this->query()
            ->select(['id', 'name', 'slug'])
            ->paginate($perPage);
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $id
     *
     * @return CategoryInterface|Model|object|null
     */
    public function getEntityForEdit(int $id): ?CategoryInterface
    {
        return $this->query()
            ->select(['id', 'name', 'slug', 'updated_at'])
            ->where('id', $id)
            ->first();
    }
}
