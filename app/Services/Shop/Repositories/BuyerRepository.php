<?php

namespace App\Services\Shop\Repositories;

use App\Models\Buyer as Model;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Repositories\BuyerRepositoryInterface;

class BuyerRepository extends Repository implements BuyerRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getCurrent(): ?BuyerInterface
    {
        $id = session()->get('buyer_id');
        if (!empty($id)) {
            return $this->getById($id);
        }
        return null;
    }

    /**
     * Возвращает покупателя по id
     *
     * @param int $id
     *
     * @return BuyerInterface|Model|object|null
     */
    public function getById(int $id): ?BuyerInterface
    {
        return $this->query()
            ->select(['id'])
            ->where(['id' => $id])
            ->first();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return Model::class;
    }
}
