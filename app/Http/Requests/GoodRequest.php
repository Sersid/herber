<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'slug' => 'nullable|string',
            'category_id' => 'required|integer|exists:shop_categories,id',
            'price' => 'required|numeric|min:0.01',
            'old_price' => 'nullable|numeric|min:0.01',
            'short_text' => 'nullable|string',
            'long_text' => 'nullable|string',
            'props.*.name' => 'required|string',
            'props.*.value' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Заполните наименование товара',
            'category_id.required'  => 'Заполните категорию',
            'price.required'  => 'Заполните цену товара',
            'props.*.name.required'  => 'Заполните название',
            'props.*.value.required'  => 'Заполните значение',
        ];
    }
}
