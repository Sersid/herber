<?php

namespace App\Services\Shop\Transformers;

use App\Helpers\PriceHelper;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Transformers\Transformer;

class GoodShowTransformer extends Transformer
{
    /**
     * @param GoodInterface $good
     *
     * @return array|mixed
     */
    public function transform($good)
    {
        return [
            'id' => $good->getId(),
            'name' => $good->getName(),
            'slug' => $good->getSlug(),
            'category_id' => $good->getCategory()
                ->getId(),
            'category' => $good->getCategory()->getName(),
            'price' => $good->getPrice(),
            'old_price' => $good->getOldPrice(),
            'short_text' => $good->getShortText(),
            'long_text' => $good->getLongText(),
            'props' => $good->getProps(),
            'updated_at' => date('d.m.Y H:i', strtotime($good->getUpdatedAt())),
        ];
    }
}
