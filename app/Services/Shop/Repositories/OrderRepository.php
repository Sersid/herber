<?php

namespace App\Services\Shop\Repositories;

use App\Models\Order as Model;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use App\Services\Shop\Interfaces\Repositories\OrderRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

class OrderRepository extends Repository implements OrderRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getLast(int $size): Collection
    {
        return $this->query()
            ->select(['id', 'created_at', 'name', 'phone', 'sum', 'status'])
            ->orderBy('status', 'asc')
            ->orderBy('id', 'desc')
            ->limit($size)
            ->get();
    }

    public function getPaginator(int $perPage): LengthAwarePaginator
    {
        return $this->query()
            ->select(['id', 'created_at', 'name', 'phone', 'sum', 'status'])
            ->orderBy('status', 'asc')
            ->orderBy('id', 'desc')
            ->paginate($perPage);
    }

    /**
     * @param int $id
     *
     * @return OrderInterface|null|Model|object
     */
    public function getEntity(int $id): ?OrderInterface
    {
        return $this->query()
            ->select(['id', 'created_at', 'name', 'phone', 'email', 'sum', 'info', 'status'])
            ->with([
                'basket' => function (HasMany $query) {
                    $query->select(['id',  'order_id', 'good_id', 'price', 'quantity']);
                },
                'basket.good' => function (HasOne $query) {
                    $query->withTrashed()->select(['id', 'name', 'slug', 'category_id']);
                },
                'basket.good.category' => function (HasOne $query) {
                    $query->withTrashed()->select(['id', 'slug']);
                },
            ])
            ->where('id', $id)
            ->first();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return Model::class;
    }
}
