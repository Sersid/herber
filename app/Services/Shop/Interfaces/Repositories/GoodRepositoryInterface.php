<?php

namespace App\Services\Shop\Interfaces\Repositories;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface GoodRepositoryInterface
{
    /**
     * Для вывода карточки товара
     *
     * @param int $id
     *
     * @return GoodInterface|null
     */
    public function getEntityForShow(int $id): ?GoodInterface;

    /**
     * Товар по id для корзаны
     *
     * @param int $id
     *
     * @return GoodInterface|null
     */
    public function getEntityForBasket(int $id): ?GoodInterface;

    /**
     * Для редактирования
     *
     * @param int $id
     *
     * @return GoodInterface|null
     */
    public function getEntityForEdit(int $id): ?GoodInterface;

    /**
     * Для вывода на главной магазина
     * @return LengthAwarePaginator|GoodInterface[]
     */
    public function getPaginate(): LengthAwarePaginator;

    /**
     * Для вывода товаров внутри категории
     *
     * @param CategoryInterface $category
     *
     * @return LengthAwarePaginator|GoodInterface[]
     */
    public function getPaginateByCategory(CategoryInterface $category): LengthAwarePaginator;

    /**
     * Для вывода в админке
     *
     * @param int $perPage
     *
     * @return LengthAwarePaginator|GoodInterface[]
     */
    public function getPaginatorForAdmin(int $perPage): LengthAwarePaginator;
}
