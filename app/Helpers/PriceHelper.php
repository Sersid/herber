<?php

namespace App\Helpers;

class PriceHelper
{
    /**
     * Форматирование цены
     * @param float|int|string $price
     *
     * @return string
     */
    public static function format($price): string
    {
        $price = explode('.', number_format((float)$price, 2, '.', ' '));
        if ($price[1] == '00') {
            unset($price[1]);
        }
        return implode('.', $price) . ' ₽';
    }
}
