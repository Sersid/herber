export default {
    props: {
        id: {
            type: Number,
            required: true
        }
    },
    data() {
        return {
            quantity: 1,
        };
    },
    computed: {
        inBasket() {
            let items = this.$store.state.basket.items;
            for (let i in items) {
                let item = items[i];
                if (item.id === this.id) {
                    return true;
                }
            }
            return false;
        }
    },
    methods: {
        add() {
            axios.post('/shop/basket/' + this.id, {quantity: this.quantity}).then(response => {
                this.$store.commit('SET_BASKET', response.data);
            });
        }
    }
}
