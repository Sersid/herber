<!-- RD Navbar Nav-->
<ul class="rd-navbar-nav">
    @foreach($items as $item)
    <li class="rd-nav-item{{ ($item['active']) ? ' active' : '' }}">
        <a class="rd-nav-link" href="{{ $item['url'] }}">{{ $item['title'] }}</a>
        @if (!empty($item['items']))
        <!-- RD Navbar Dropdown-->
        <ul class="rd-menu rd-navbar-dropdown">
            @foreach($item['items'] as $child)
            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="{{ $child['url'] }}">{{ $child['title'] }}</a></li>
            @endforeach
        </ul>
        @endif
    </li>
    @endforeach
</ul>
