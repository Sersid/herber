<!-- Product-->
<article class="product">
    <div class="product-figure">
        <img src="{{ $good->getThumbnail() }}" alt="" width="270" height="280"/>
        <add-to-basket :id="{{ $good->getId() }}"/>
    </div>
    <h5 class="product-title"><a href="{{ $good->getUrl() }}">{{ $good->getName() }}</a>
    </h5>
    <div class="product-price-wrap">
        @if ($good->hasOldPrice())
            <div class="product-price product-price-old">{{ $good->getFormattedOldPrice() }}</div>
        @endif
        <div class="product-price">{{ $good->getFormattedPrice() }}</div>
    </div>
</article>
