<?php

namespace App\Services\Shop\Repositories;

use App\Models\BasketItem as Model;
use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Repositories\BasketRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;

class BasketRepository extends Repository implements BasketRepositoryInterface
{
    /**
     * @inheritDoc
     * @return BasketItemInterface|null|Model
     */
    public function getBasketItem(BuyerInterface $buyer, GoodInterface $good): ?BasketItemInterface
    {
        $item = $this->query()
            ->select(['id', 'quantity', 'good_id', 'buyer_id', 'price'])
            ->where(['good_id' => $good->getId(), 'buyer_id' => $buyer->getId(), 'order_id' => null])
            ->first();
        if ($item instanceof BasketItemInterface) {
            return $item;
        }
        return null;
    }

    /**
     * @inheritDoc
     * @return Collection|BasketItemInterface[]
     */
    public function getAllByBuyer(BuyerInterface $buyer)
    {
        return $this->query()
            ->select(['id', 'quantity', 'good_id', 'buyer_id', 'price'])
            ->with([
                'good' => function (HasOne $query) {
                    $query->select(['id', 'name', 'price', 'slug', 'category_id']);
                },
                'good.category' => function (HasOne $query) {
                    $query->select(['id', 'name', 'slug']);
                },
            ])
            ->where(['buyer_id' => $buyer->getId(), 'order_id' => null])
            ->get();
    }

    /**
     * @param BuyerInterface $buyer
     *
     * @return Collection|BasketItemInterface[]
     */
    public function getForCheckout(BuyerInterface $buyer)
    {
        return $this->query()
            ->select(['id', 'quantity', 'good_id', 'buyer_id', 'price'])
            ->where(['buyer_id' => $buyer->getId(), 'order_id' => null])
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function getSumByBuyer(BuyerInterface $buyer): float
    {
        $sum = 0;;
        foreach ($this->getForCheckout($buyer) as $basketItem) {
            $sum += $basketItem->getSum();
        }
        return $sum;
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return Model::class;
    }
}
