<?php

namespace App\Observers;

use App\Models\Category;
use Cache;

class CategoryObserver
{
    /**
     * Handle the good "created" event.
     *
     * @param Category $category
     *
     * @return void
     */
    public function created(Category $category)
    {
        $this->clearCache();
    }

    /**
     * Clear cache
     */
    private function clearCache()
    {
        Cache::tags(['categories', 'goods'])
            ->flush();
    }

    /**
     * Handle the good "updated" event.
     *
     * @param Category $category
     *
     * @return void
     */
    public function updated(Category $category)
    {
        $this->clearCache();
    }

    /**
     * Handle the good "deleted" event.
     *
     * @param Category $category
     *
     * @return void
     */
    public function deleted(Category $category)
    {
        $this->clearCache();
    }

    /**
     * Handle the good "restored" event.
     *
     * @param Category $category
     *
     * @return void
     */
    public function restored(Category $category)
    {
        $this->clearCache();
    }

    /**
     * Handle the good "force deleted" event.
     *
     * @param Category $category
     *
     * @return void
     */
    public function forceDeleted(Category $category)
    {
        $this->clearCache();
    }
}
