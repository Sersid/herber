<?php

namespace App\Services\Shop\Interfaces\Repositories;

use App\Services\Shop\Interfaces\Entities\OrderInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface OrderRepositoryInterface
{
    /**
     * Новые и последние заказы
     *
     * @param int $size
     *
     * @return OrderInterface[]|Collection
     */
    public function getLast(int $size): Collection;

    /**
     * Заказы с постраничной навигацией
     *
     * @param int $perPage
     *
     * @return LengthAwarePaginator
     */
    public function getPaginator(int $perPage): LengthAwarePaginator;

    /**
     * Заказ по id
     *
     * @param int $id
     *
     * @return OrderInterface|null
     */
    public function getEntity(int $id): ?OrderInterface;
}
