<?php

namespace App\Services\Shop\Interfaces;

use App\Services\Shop\Interfaces\Entities\OrderInterface;

/**
 * Действия с заказов
 * @package App\Services\Shop
 */
interface OrderServiceInterface
{
    /**
     * Создание заказа
     *
     * @param float       $sum
     * @param string      $name
     * @param string      $phone
     * @param string|null $email
     * @param string|null $info
     *
     * @return OrderInterface
     */
    public function checkout(float $sum, string $name, string $phone, ?string $email, ?string $info): OrderInterface;

    /**
     * Обновляет статус заказа
     *
     * @param OrderInterface $order
     * @param                $status
     *
     * @return mixed
     */
    public function updateStatus(OrderInterface $order, $status);
}
