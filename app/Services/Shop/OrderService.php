<?php

namespace App\Services\Shop;

use App\Models\Order;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use App\Services\Shop\Interfaces\OrderServiceInterface;
use Exception;

class OrderService implements OrderServiceInterface
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function checkout(float $sum, string $name, string $phone, ?string $email, ?string $info): OrderInterface
    {
        return Order::create([
            'sum' => $sum,
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
            'info' => $info,
            'status' =>  Order::STATUS_NEW,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function updateStatus(OrderInterface $order, $status)
    {
        if ($order->getStatus() != $status) {
            Order::find($order->getId())
                ->update(['status' => $status]);
        }
    }
}
