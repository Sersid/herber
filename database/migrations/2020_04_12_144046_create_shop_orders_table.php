<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopOrdersTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('shop_orders',
            function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->integer('status')
                    ->index();
                $table->unsignedDecimal('sum')
                    ->index();
                $table->string('name');
                $table->string('phone');
                $table->string('email')
                    ->nullable();
                $table->text('info')
                    ->nullable();
            });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_orders');
    }
}
