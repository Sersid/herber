<?php

namespace App\Services\Shop\Transformers;

use App\Helpers\PriceHelper;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use App\Transformers\Transformer;

class OrderTransformer extends Transformer
{
    /**
     * @param OrderInterface $order
     *
     * @return array|mixed
     */
    public function transform($order)
    {
        return [
            'id' => $order->getId(),
            'number' => $order->getNumber(),
            'created_at' => date('d.m.Y H:i:s', strtotime($order->getCreatedAt())),
            'name' => $order->getName(),
            'phone' => $order->getPhone(),
            'sum' => PriceHelper::format($order->getSum()),
            'is_new' => $order->isNew(),
        ];
    }
}
