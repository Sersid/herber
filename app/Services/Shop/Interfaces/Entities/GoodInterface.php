<?php

namespace App\Services\Shop\Interfaces\Entities;

interface GoodInterface
{
    /**
     * Id
     * @return int
     */
    public function getId(): int;

    /**
     * Название товара
     * @return string
     */
    public function getName(): string;

    /**
     * Slug
     * @return string
     */
    public function getSlug(): string;

    /**
     * Url
     * @return string
     */
    public function getUrl(): string;

    /**
     * Цена
     * @return float
     */
    public function getPrice(): float;

    /**
     * Указана ли старая цена
     * @return bool
     */
    public function hasOldPrice(): bool;

    /**
     * Старая цена
     * @return float|null
     */
    public function getOldPrice(): ?float;

    /**
     * Загружена ли миниатюра (фотография)
     * @return bool
     */
    public function hasThumbnail(): bool;

    /**
     * Миниатюра (фотография)
     * @return string|null
     */
    public function getThumbnail(): ?string;

    /**
     * Указано ли краткое описание
     * @return bool
     */
    public function hasShortText(): bool;

    /**
     * Краткое описание
     * @return string|null
     */
    public function getShortText(): ?string;

    /**
     * Указаны ли свойства
     * @return bool
     */
    public function hasProps(): bool;

    /**
     * Свойства товара
     * @return array|null
     */
    public function getProps(): ?array;

    /**
     * Указано ли подробное описание
     * @return bool
     */
    public function hasLongText(): bool;

    /**
     * Подробное описание
     * @return string|null
     */
    public function getLongText(): ?string;

    /**
     * Указаны ли сопутствующие товары
     * @return bool
     */
    public function hasRelatedGoods(): bool;

    /**
     * Сопутствующие товары
     * @return GoodInterface[]
     */
    public function getRelatedGoods(): ?array;

    /**
     * Есть ли галарея изображений
     * @return bool
     */
    public function hasImages(): bool;

    /**
     * Галерея изображений
     * @return ImageInterface[]|null
     */
    public function getImages(): ?array;

    /**
     * Категория
     * @return CategoryInterface
     */
    public function getCategory(): CategoryInterface;

    /**
     * Цена для вывода
     * @return string
     */
    public function getFormattedPrice(): string;

    /**
     * Старая цена для вывода
     * @return string
     */
    public function getFormattedOldPrice(): ?string;

    /**
     * Дата создания
     * @return string
     */
    public function getCreatedAt(): ?string;

    /**
     * Дата обновления
     * @return string
     */
    public function getUpdatedAt(): ?string;
}
