window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + api_token;
window.axios.defaults.headers.common['Accept'] = 'application/json';
window.axios.defaults.baseURL = '/api/';
import Vue from 'vue';
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import Home from './components/Home'
import Orders from './components/Shop/Orders';
import OrderDetail from './components/Shop/OrderDetail';
import Goods from './components/Shop/Goods';
import GoodEdit from './components/Shop/GoodEdit';
import GoodCreate from './components/Shop/GoodCreate';
import Categories from './components/Shop/Categories';
import CategoryCreate from './components/Shop/CategoryCreate';
import CategoryEdit from './components/Shop/CategoryEdit';

Vue.use(VueRouter);
Vue.use(BootstrapVue);

let router = new VueRouter({
    routes: [
        {path: '/', component: Home, name: 'home'},
        {
            path: '/orders/:id',
            name: 'order-detail',
            component: OrderDetail,
        },
        {
            path: '/orders',
            name: 'orders',
            component: Orders,
        },
        {
            path: '/goods/create',
            name: 'good-create',
            component: GoodCreate,
        },
        {
            path: '/goods/edit/:id',
            name: 'good-edit',
            component: GoodEdit,
        },
        {
            path: '/goods',
            name: 'goods',
            component: Goods,
        },
        {
            path: '/categories/create',
            name: 'category-create',
            component: CategoryCreate,
        },
        {
            path: '/categories/edit/:id',
            name: 'category-edit',
            component: CategoryEdit,
        },
        {
            path: '/categories',
            name: 'categories',
            component: Categories,
        },
    ]
});

new Vue({
    el: '#app',
    router: router,
    //store,
    render: h => h(App)
});
