<?php

namespace App\Services\Shop\Interfaces;

use App\Services\Shop\Interfaces\Entities\GoodInterface;

interface GoodServiceInterface
{
    /**
     * Создание товара
     *
     * @param array $data
     *
     * @return GoodInterface
     */
    public function create(array $data): GoodInterface;

    /**
     * Обновление товара
     *
     * @param GoodInterface $good
     * @param array         $data
     *
     * @return GoodInterface
     */
    public function update(GoodInterface $good, array $data);

    /**
     * Удаление товара
     *
     * @param GoodInterface $good
     */
    public function remove(GoodInterface $good);

    /**
     * Копирует товар
     *
     * @param GoodInterface $good
     *
     * @return GoodInterface
     */
    public function copy(GoodInterface $good): GoodInterface;
}
