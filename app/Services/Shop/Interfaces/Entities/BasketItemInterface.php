<?php

namespace App\Services\Shop\Interfaces\Entities;

interface BasketItemInterface
{
    /**
     * Id
     * @return int
     */
    public function getId(): int;

    /**
     * Стоимость за единицу товара
     * @return float
     */
    public function getPrice(): float;

    /**
     * Кол-во единиц товара в корзине
     * @return int
     */
    public function getQuantity(): int;

    /**
     * Сумма стоимости на количество
     * @return float
     */
    public function getSum(): float;

    /**
     * Товар
     * @return GoodInterface
     */
    public function getGood(): ?GoodInterface;

    /**
     * Покупатель
     * @return BuyerInterface
     */
    public function getBuyer(): BuyerInterface;
}
