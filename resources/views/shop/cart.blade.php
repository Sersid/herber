@extends('template')
@section('content')
    @include('widgets.breadcrumbs', [
        'items' => [
            [
                'name' => 'Каталог продукции',
                'url' => url('shop')
            ],
    ],
        'title' => 'Оформить заказ',
    ])
    <cart></cart>
@endsection
