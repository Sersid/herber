<?php

namespace App\Services\Shop;

use App\Models\Good;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\GoodServiceInterface;
use App\Services\Shop\Interfaces\ImageServiceInterface;

class GoodService implements GoodServiceInterface
{
    private ImageServiceInterface $imageService;

    public function __construct(ImageServiceInterface $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): GoodInterface
    {
        $good = Good::create($data);
        $this->imageService->updateGoodForEmpty($good);
        return $good;
    }

    /**
     * @inheritDoc
     */
    public function update(GoodInterface $good, array $data)
    {
        $this->imageService->updateGoodForEmpty($good);
        return Good::find($good->getId())
            ->update($data);
    }

    /**
     * @inheritDoc
     */
    public function remove(GoodInterface $good)
    {
        Good::find($good->getId())
            ->delete();
    }

    /**
     * @inheritDoc
     */
    public function copy(GoodInterface $good): GoodInterface
    {
        $newGood = Good::create([
            'name' => $good->getName(),
            'slug' => $good->getSlug(),
            'category_id' => $good->getCategory()
                ->getId(),
            'price' => $good->getPrice(),
            'old_price' => $good->getOldPrice(),
            'short_text' => $good->getShortText(),
            'long_text' => $good->getLongText(),
            'props' => $good->getProps(),
        ]);
        $this->imageService->copy($good, $newGood);
        return $newGood;
    }
}
