<?php

namespace App\Models;

use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @package App\Services\Shop\Entities
 * @property int                              id
 * @property int                              status
 * @property float                            sum
 * @property string                           name
 * @property string                           phone
 * @property string|null                      email
 * @property string|null                      info
 * @property BasketItemInterface[]|Collection basket
 * @property string                           created_at
 */
class Order extends BaseModel implements OrderInterface
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'shop_orders';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['status', 'sum', 'name', 'phone', 'email', 'info'];

    /**
     * Содержимое корзины
     * @return HasMany
     */
    public function basket()
    {
        return $this->hasMany(BasketItem::class, 'order_id', 'id');
    }

    /**
     * @inheritDoc
     */
    public function getBasket(): array
    {
        return $this->basket->all();
    }

    /**
     * @inheritDoc
     */
    public function getNumber(): string
    {
        return str_repeat(0, 6 - strlen($this->getId())) . $this->getId();
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @inheritDoc
     */
    public function getStatusName(): string
    {
        return $this->getStatuses()[$this->getStatus()];
    }

    /**
     * Статусы заказов
     * @return array|string[]
     */
    public function getStatuses(): array
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_IN_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнен',
            self::STATUS_CANCEL => 'Отменен',
        ];
    }

    /**
     * @inheritDoc
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @inheritDoc
     */
    public function isNew(): bool
    {
        return $this->getStatus() == self::STATUS_NEW;
    }

    /**
     * @inheritDoc
     */
    public function getSum(): float
    {
        return $this->sum;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }
}
