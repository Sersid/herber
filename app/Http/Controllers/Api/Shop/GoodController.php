<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\ApiController;
use App\Http\Requests\GoodRequest;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\GoodServiceInterface as Service;
use App\Services\Shop\Interfaces\Repositories\GoodRepositoryInterface as Repository;
use App\Services\Shop\Transformers\GoodShowTransformer;
use App\Services\Shop\Transformers\GoodTransformer as Transformer;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class GoodController extends ApiController
{
    protected Repository $repository;
    protected Service $service;

    public function __construct(Repository $repository, Service $service, Transformer $transformer)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Список товаров с постраничной навигацией
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $paginator = $this->repository->getPaginatorForAdmin(50);
        return $this->respondWithPaginator($paginator);
    }

    /**
     * Товар для редактирования
     *
     * @param int                 $id
     * @param GoodShowTransformer $transformer
     *
     * @return JsonResponse
     */
    public function show(int $id, GoodShowTransformer $transformer): JsonResponse
    {
        try {
            $this->transformer = $transformer;
            return $this->respondWithTransformer($this->getEntity($id));
        } catch (NotFoundHttpException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * @param int $id
     *
     * @return GoodInterface
     * @throws NotFoundHttpException
     */
    private function getEntity(int $id): GoodInterface
    {
        $good = $this->repository->getEntityForEdit($id);
        if (empty($good)) {
            throw new NotFoundHttpException('Good not found');
        }
        return $good;
    }

    /**
     * Создание товара
     *
     * @param GoodRequest $request
     *
     * @return JsonResponse
     */
    public function store(GoodRequest $request): JsonResponse
    {
        $good = $this->service->create($this->getDataFromRequest($request));
        return $this->respondCreated(['item' => ['id' => $good->getId()]]);
    }

    /**
     * @param GoodRequest $request
     *
     * @return array
     */
    private function getDataFromRequest(GoodRequest $request): array
    {
        return [
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'category_id' => $request->get('category_id'),
            'price' => $request->get('price'),
            'old_price' => $request->get('old_price'),
            'short_text' => $request->get('short_text'),
            'long_text' => $request->get('long_text'),
            'props' => array_values($request->get('props')),
        ];
    }

    /**
     * Обновляет товар
     *
     * @param int         $id
     * @param GoodRequest $request
     *
     * @return JsonResponse
     */
    public function update(int $id, GoodRequest $request): JsonResponse
    {
        try {
            $this->service->update($this->getEntity($id), $this->getDataFromRequest($request));
            return $this->respondSuccess();
        } catch (NotFoundHttpException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * Удаление товара
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function remove(int $id): JsonResponse
    {
        try {
            $this->service->remove($this->getEntity($id));
            return $this->respondSuccess();
        } catch (NotFoundHttpException $e) {
            return $this->respondNotFound($e->getMessage());
        }
    }

    /**
     * Копирование товара
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function copy(int $id): JsonResponse
    {
        $good = $this->getEntity($id);
        $newGood = $this->service->copy($good);
        return $this->respondWithTransformer($newGood);
    }
}
