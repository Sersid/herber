<?php

namespace App\Services\Shop;

use App\Models\BasketItem;
use App\Services\Shop\Interfaces\BasketServiceInterface;
use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use App\Services\Shop\Interfaces\Repositories\BasketRepositoryInterface;
use Exception;

/**
 * Действия с содержимым корзины
 * @package App\Services\Shop
 */
class BasketService implements BasketServiceInterface
{
    protected BasketRepositoryInterface $basketItemRepository;

    /**
     * Basket constructor.
     *
     * @param BasketRepositoryInterface $basketItemRepository
     */
    public function __construct(BasketRepositoryInterface $basketItemRepository)
    {
        $this->basketItemRepository = $basketItemRepository;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function add(BuyerInterface $buyer, GoodInterface $good, int $quantity)
    {
        BasketItem::create([
            'buyer_id' => $buyer->getId(),
            'good_id' => $good->getId(),
            'price' => $good->getPrice(),
            'quantity' => $quantity < 1 ? 1 : $quantity,
        ]);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function updateQuantity(BasketItemInterface $basketItem, int $quantity)
    {
        BasketItem::find($basketItem->getId())
            ->update(['quantity' => $quantity]);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function remove(BasketItemInterface $basketItem)
    {
        BasketItem::find($basketItem->getId())
            ->delete();
    }

    /**
     * @inheritDoc
     */
    public function updatePrice(BasketItemInterface $basketItem, float $price)
    {
        BasketItem::find($basketItem->getId())
            ->update(['price' => $price]);
    }

    /**
     * @inheritDoc
     */
    public function updateOrder(BuyerInterface $buyer, OrderInterface $order)
    {
        BasketItem::where('buyer_id', $buyer->getId())
            ->where('order_id', null)
            ->update(['order_id' => $order->getId()]);
    }
}
