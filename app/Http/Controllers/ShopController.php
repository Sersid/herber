<?php

namespace App\Http\Controllers;

use App\Services\Shop\Interfaces\Repositories\CategoryRepositoryInterface;
use App\Services\Shop\Interfaces\Repositories\GoodRepositoryInterface;
use Illuminate\View\View;

/**
 * Class ShopController
 * @package App\Http\Controllers
 */
final class ShopController extends Controller
{
    protected GoodRepositoryInterface $goodRepository;
    protected CategoryRepositoryInterface $categoryRepository;

    /**
     * @param GoodRepositoryInterface $goodRepository
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        GoodRepositoryInterface $goodRepository,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->goodRepository = $goodRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Главная раздела
     * @return View
     */
    public function index()
    {
        $paginate = $this->goodRepository->getPaginate();
        $categories = $this->categoryRepository->getForFilter();
        return view('shop.index', compact('paginate', 'categories'));
    }

    /**
     * @param string $categotySlug
     * @param int    $id
     *
     * @return View
     */
    public function show(string $categotySlug, int $id)
    {
        $good = $this->goodRepository->getEntityForShow($id);
        if (empty($good)) {
            abort(404, 'Товар не найден');
        }
        return view('shop.show', compact('good'));
    }

    /**
     * @param string $slug
     *
     * @return View
     */
    public function category(string $slug)
    {
        $category = $this->categoryRepository->getEntityBySlug($slug);
        if (empty($category)) {
            abort(404, 'Категория не найдена');
        }
        $paginate = $this->goodRepository->getPaginateByCategory($category);
        return view('shop.category', compact('category', 'paginate'));
    }
}
