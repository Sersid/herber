<?php

namespace App\Services\Shop\Interfaces\Repositories;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface CategoryRepositoryInterface
{
    /**
     * @param int $perPage
     *
     * @return CategoryInterface[]|LengthAwarePaginator
     */
    public function getPaginatorForAdmin(int $perPage): LengthAwarePaginator;

    /**
     * @return CategoryInterface[]|array
     */
    public function getForMenu(): array;

    /**
     * @return array|CategoryInterface[]
     */
    public function getForFilter(): array;

    /**
     * @return Collection|CategoryInterface[]
     */
    public function getForDropdown(): Collection;

    /**
     * @param string $slug
     *
     * @return CategoryInterface|null
     */
    public function getEntityBySlug(string $slug): ?CategoryInterface;

    /**
     * Для редактирования
     *
     * @param int $id
     *
     * @return CategoryInterface|null
     */
    public function getEntityForEdit(int $id): ?CategoryInterface;
}
