<?php

namespace App\Services\Shop\Transformers;

use App\Helpers\PriceHelper;
use App\Services\Shop\Interfaces\Entities\OrderInterface;
use App\Transformers\Transformer;

class OrderShowTransformer extends Transformer
{
    /**
     * @param OrderInterface $order
     *
     * @return array|mixed
     */
    public function transform($order)
    {
        $result = [
            'id' => $order->getId(),
            'number' => $order->getNumber(),
            'created_at' => date('d.m.Y H:i:s', strtotime($order->getCreatedAt())),
            'sum' => PriceHelper::format($order->getSum()),
            'name' => $order->getName(),
            'phone' => $order->getPhone(),
            'email' => $order->getEmail(),
            'info' => $order->getInfo(),
            'basket' => [],
        ];
        foreach ($order->getBasket() as $basketItem) {
            $result['basket'][] = [
                'name' => $basketItem->getGood()->getName(),
                'url' => $basketItem->getGood()->getUrl(),
                'price' => PriceHelper::format($basketItem->getPrice()),
                'quantity' => $basketItem->getQuantity(),
                'sum' => PriceHelper::format($basketItem->getSum()),
            ];
        }
        return $result;
    }
}
