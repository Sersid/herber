<?php

namespace App\Services\Shop\Interfaces;

use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\OrderInterface;

/**
 * Действия с содержимым корзины
 * @package App\Services\Shop
 */
interface BasketServiceInterface
{
    /**
     * Добавляет товар в корзину
     *
     * @param BuyerInterface $buyer
     * @param GoodInterface  $good
     * @param int|null       $quantity
     */
    public function add(BuyerInterface $buyer, GoodInterface $good, int $quantity);

    /**
     * Обновляет кол-во необходимого товара
     *
     * @param BasketItemInterface $basketItem
     * @param int                 $quantity
     */
    public function updateQuantity(BasketItemInterface $basketItem, int $quantity);

    /**
     * Обновляет цену товара в корзине
     *
     * @param BasketItemInterface $basketItem
     * @param float               $price
     */
    public function updatePrice(BasketItemInterface $basketItem, float $price);

    /**
     * Обновляет номер заказа у товаров в корзине
     *
     * @param BuyerInterface $buyer
     * @param OrderInterface $order
     */
    public function updateOrder(BuyerInterface $buyer, OrderInterface $order);

    /**
     * Удаляет из корзины
     *
     * @param BasketItemInterface $basketItemd
     */
    public function remove(BasketItemInterface $basketItemd);
}
