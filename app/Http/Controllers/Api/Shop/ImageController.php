<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\ApiController;
use App\Http\Requests\UploadImageRequest;
use App\Services\Shop\Interfaces\ImageServiceInterface;
use App\Services\Shop\Interfaces\Repositories\ImageRepositoryInterface;
use App\Services\Shop\Transformers\ImageTransformer;
use Illuminate\Http\JsonResponse;

final class ImageController extends ApiController
{
    private ImageServiceInterface $service;
    private ImageRepositoryInterface $repository;

    public function __construct(
        ImageRepositoryInterface $repository,
        ImageServiceInterface $service,
        ImageTransformer $transformer
    ) {
        $this->service = $service;
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    /**
     * Список товаров для формы
     * @return JsonResponse
     */
    public function list()
    {
        $images = $this->repository->getForForm((int)request()->get('good_id'));
        return $this->respondWithTransformer($images);
    }

    /**
     * Загрузка изображений
     *
     * @param UploadImageRequest $request
     *
     * @return JsonResponse
     */
    public function upload(UploadImageRequest $request)
    {
        $image = $this->service->upload($request->file('image'), $request->get('good_id'));
        return $this->respondWithTransformer($image, 201);
    }

    /**
     * Удалить изображение
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function remove(int $id)
    {
        $image = $this->repository->getEntity($id);
        if (empty($image)) {
            return $this->respondNotFound('Image not found');
        }
        $this->service->remove($image);
        return $this->respondSuccess();
    }
}
