<?php

namespace App\Services\Shop\Repositories;

use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\ImageInterface;
use App\Services\Shop\Interfaces\Repositories\ImageRepositoryInterface;
use App\Models\Image as Model;
use Illuminate\Support\Collection;

class ImageRepository extends Repository implements ImageRepositoryInterface
{
    /**
     * @inheritDoc
     * @return ImageInterface|Model|object|null
     */
    public function getEntity(int $id): ?ImageInterface
    {
        return $this->query()
            ->select(['id', 'good_id', 'sm', 'md', 'lg'])
            ->where('id', $id)
            ->first();
    }

    /**
     * @inheritDoc
     */
    public function getForForm(?int $goodId): Collection
    {
        $query = $this->query()
            ->select(['id', 'good_id', 'name', 'sm', 'md', 'lg'])
            ->where('good_id', null);

        if (!empty($goodId)) {
            $query->orWhere('good_id', $goodId);
        }
        return $query->get();
    }

    /**
     * @inheritDoc
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @inheritDoc
     */
    public function getByGood(GoodInterface $good): Collection
    {
        return $this->query()
            ->select(['id', 'good_id', 'name', 'sm', 'md', 'lg'])
            ->where('good_id', $good->getId())
            ->get();
    }
}
