<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Admin',
                'email' => 'admin@admin.ru',
                'password' => '$2y$10$rJ0RfkWinuuc2iT63KBy4.0zQ4P3m0AHhuZMpV1Y3GZBbn.USzqx.',
                'api_token' => Str::random(80),
            ]
        );
    }
}
