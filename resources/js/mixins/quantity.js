export default {
    data() {
        return {
            quantity: 0
        };
    },
    methods: {
        setQuantity(val) {
            this.quantity += parseInt(val);
        },
    }
}

