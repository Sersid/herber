<?php

namespace App\Models;

use App\Helpers\PriceHelper;
use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\ImageInterface;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @package App\Services\Shop\Entities
 * @property int               id
 * @property string            name
 * @property string            slug
 * @property integer           category_id
 * @property CategoryInterface category
 * @property ImageInterface    thumbnail
 * @property float             price
 * @property float|null        old_price
 * @property string            short_text
 * @property string            long_text
 * @property array|null        props
 * @property string            created_at
 * @property string            updated_at
 * @property ImageInterface[]|Collection  images
 */
class Good extends BaseModel implements GoodInterface
{
    use SoftDeletes, Sluggable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'shop_goods';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['name', 'slug', 'category_id', 'price', 'old_price', 'short_text', 'long_text', 'props'];

    /**
     * The attributes that should be cast.
     * @var array
     */
    protected $casts = ['props' => 'json'];

    /**
     * Категория
     * @return HasOne
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * Миниатюра (фотография)
     * @return HasOne
     */
    public function thumbnail()
    {
        return $this->hasOne(Image::class, 'good_id', 'id');
    }

    /**
     * Изображенния
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class, 'good_id', 'id');
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return url('shop/' . $this->category->getSlug() . '/' . $this->getId() . '-' . $this->getSlug());
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @inheritDoc
     */
    public function getThumbnail(): ?string
    {
        return $this->hasThumbnail() ? $this->thumbnail->getMedium() : '/images/no-photo.png';
    }

    /**
     * @inheritDoc
     */
    public function hasThumbnail(): bool
    {
        return !empty($this->thumbnail);
    }

    /**
     * @inheritDoc
     */
    public function getRelatedGoods(): ?array
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function hasRelatedGoods(): bool
    {
        // @todo доделать
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getShortText(): ?string
    {
        return $this->hasShortText() ? $this->short_text : null;
    }

    /**
     * @inheritDoc
     */
    public function hasShortText(): bool
    {
        return !empty(trim($this->short_text));
    }

    /**
     * @inheritDoc
     */
    public function getProps(): ?array
    {
        return $this->props;
    }

    /**
     * @inheritDoc
     */
    public function hasProps(): bool
    {
        return !empty($this->props);
    }

    /**
     * @inheritDoc
     */
    public function getLongText(): ?string
    {
        return $this->hasLongText() ? $this->long_text : null;
    }

    /**
     * @inheritDoc
     */
    public function hasLongText(): bool
    {
        return !empty(trim($this->long_text));
    }

    /**
     * @inheritDoc
     */
    public function getImages(): ?array
    {
        return $this->hasImages() ? $this->images->all() : null;
    }

    /**
     * @inheritDoc
     */
    public function hasImages(): bool
    {
        return !empty($this->images) && $this->images->count() > 0;
    }

    /**
     * @inheritDoc
     */
    public function getCategory(): CategoryInterface
    {
        return $this->category;
    }

    /**
     * @inheritDoc
     */
    public function getFormattedPrice(): string
    {
        return PriceHelper::format($this->getPrice());
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function getFormattedOldPrice(): ?string
    {
        return PriceHelper::format($this->getOldPrice());
    }

    /**
     * @inheritDoc
     */
    public function getOldPrice(): ?float
    {
        return $this->old_price;
    }

    /**
     * @inheritDoc
     */
    public function hasOldPrice(): bool
    {
        return is_float($this->getOldPrice()) && $this->getOldPrice() > $this->getPrice();
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updated_at;
    }
}
