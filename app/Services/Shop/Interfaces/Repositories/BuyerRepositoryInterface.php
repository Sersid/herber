<?php

namespace App\Services\Shop\Interfaces\Repositories;

use App\Services\Shop\Interfaces\Entities\BuyerInterface;

interface BuyerRepositoryInterface
{
    /**
     * Возвращает текущего покупателя
     * @return BuyerInterface|null
     */
    public function getCurrent(): ?BuyerInterface;
}
