<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

final class PageController extends Controller
{
    /**
     * Главная страница
     * @return View
     */
    public function index()
    {
        return view('page/index');
    }
}
