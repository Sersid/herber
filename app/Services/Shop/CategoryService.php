<?php

namespace App\Services\Shop;

use App\Models\Category;
use App\Services\Shop\Interfaces\CategoryServiceInterface;
use App\Services\Shop\Interfaces\Entities\CategoryInterface;

class CategoryService implements CategoryServiceInterface
{
    /**
     * @inheritDoc
     */
    public function create(array $data): CategoryInterface
    {
        return Category::create($data);
    }

    /**
     * @inheritDoc
     */
    public function update(CategoryInterface $category, array $data)
    {
        return Category::find($category->getId())
            ->update($data);
    }

    /**
     * @inheritDoc
     */
    public function remove(CategoryInterface $category)
    {
        Category::find($category->getId())
            ->delete();
    }
}
