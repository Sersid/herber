<?php

namespace App\Widgets;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Repositories\CategoryRepositoryInterface;
use Arrilot\Widgets\AbstractWidget;

/**
 * @package App\Widgets
 */
class MainMenu extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.main_menu', ['items' => $this->getItems()]);
    }

    /**
     * @return array
     */
    protected function getItems(): array
    {
        return [
            [
                'title' => 'Главная',
                'url' => '/',
                'active' => request()->is('/'),
            ],
            [
                'title' => 'О нас',
                'url' => '/o-nas',
                'active' => request()->is('o-nas'),
            ],
            [
                'title' => 'Каталог продукции',
                'url' => '/shop',
                'active' => request()->is('shop') || request()->is('shop/*'),
                'items' => array_map(
                    function ($item) {
                        /* @var CategoryInterface $item */
                        return ['title' => $item->getName(), 'url' => $item->getUrl()];
                    },
                    app(CategoryRepositoryInterface::class)->getForMenu()
                ),
            ],
            [
                'title' => 'Доставка',
                'url' => '/dostavka',
                'active' => request()->is('dostavka'),
            ],
            [
                'title' => 'Контакты',
                'url' => '/kontakty',
                'active' => request()->is('kontakty'),
            ],
        ];
    }
}
