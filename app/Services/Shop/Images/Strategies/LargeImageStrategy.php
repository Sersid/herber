<?php

namespace App\Services\Shop\Images\Strategies;

class LargeImageStrategy implements ImageStrategyInterface
{
    /**
     * @inheritDoc
     */
    function getSuffix(): string
    {
        return 'lg';
    }

    /**
     * @inheritDoc
     */
    function getWidth(): ?int
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    function getHeight(): ?int
    {
        return 600;
    }
}
