<?php

namespace App\Services\Shop\Repositories;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Repositories\GoodRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CachedGoodRepository extends CachedRepository implements GoodRepositoryInterface
{
    protected GoodRepositoryInterface $repository;

    public function __construct(GoodRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function getEntityForShow(int $id): ?GoodInterface
    {
        return $this->getCache('show-' . $id, fn() => $this->repository->getEntityForShow($id));
    }

    /**
     * @inheritDoc
     */
    public function getEntityForBasket(int $id): ?GoodInterface
    {
        return $this->getCache('basket-' . $id, fn() => $this->repository->getEntityForBasket($id));
    }

    /**
     * @inheritDoc
     */
    public function getEntityForEdit(int $id): ?GoodInterface
    {
        return $this->repository->getEntityForEdit($id);
    }

    /**
     * @inheritDoc
     */
    public function getPaginate(): LengthAwarePaginator
    {
        return $this->getCachePaginate('paginate', fn() => $this->repository->getPaginate());
    }

    /**
     * @inheritDoc
     */
    public function getPaginateByCategory(CategoryInterface $category): LengthAwarePaginator
    {
        return $this->getCachePaginate(
            'paginate-by-category-' . $category->getId(),
            fn() => $this->repository->getPaginateByCategory($category)
        );
    }

    /**
     * @inheritDoc
     */
    public function getPaginatorForAdmin(int $perPage): LengthAwarePaginator
    {
        return $this->repository->getPaginatorForAdmin($perPage);
    }

    /**
     * @inheritDoc
     */
    protected function tag(): string
    {
        return 'goods';
    }

    /**
     * @inheritDoc
     */
    protected function durations(): int
    {
        return 3600;
    }
}
