<?php

namespace App\Http\Controllers;

use App\Helpers\PriceHelper;
use App\Http\Requests\BasketRequest;
use App\Services\Shop\Interfaces\BasketServiceInterface;
use App\Services\Shop\Interfaces\BuyerServiceInterface;
use App\Services\Shop\Interfaces\Entities\BasketItemInterface;
use App\Services\Shop\Interfaces\Entities\BuyerInterface;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Repositories\BasketRepositoryInterface;
use App\Services\Shop\Interfaces\Repositories\BuyerRepositoryInterface;
use App\Services\Shop\Interfaces\Repositories\GoodRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * @package App\Http\Controllers
 */
final class BasketController extends Controller
{
    protected GoodRepositoryInterface $goodRepository;
    protected BasketServiceInterface $basketService;
    protected BasketRepositoryInterface $basketRepository;
    protected BuyerRepositoryInterface $buyerRepository;
    protected BuyerServiceInterface $buyerService;

    public function __construct(
        GoodRepositoryInterface $goodRepository,
        BasketRepositoryInterface $basketRepository,
        BuyerRepositoryInterface $buyerRepository,
        BasketServiceInterface $basketService,
        BuyerServiceInterface $buyerService
    ) {
        $this->goodRepository = $goodRepository;
        $this->basketRepository = $basketRepository;
        $this->buyerRepository = $buyerRepository;
        $this->basketService = $basketService;
        $this->buyerService = $buyerService;
    }

    /**
     * Добавление товара в корзину
     *
     * @param int           $id
     * @param BasketRequest $request
     *
     * @return JsonResponse
     */
    public function add(int $id, BasketRequest $request): JsonResponse
    {
        $basketItem = $this->getBasketItem($id, false);
        $quantity = $request->get('quantity');
        if (empty($basketItem)) {
            $this->basketService->add($this->getBuyer(), $this->getGood($id), $quantity);
        } else {
            $this->basketService->updateQuantity($basketItem, $basketItem->getQuantity() + $quantity);
        }
        return $this->index();
    }

    /**
     * Возвращает позицию в корзине по id товара
     *
     * @param int  $id
     * @param bool $required
     *
     * @return BasketItemInterface
     */
    protected function getBasketItem(int $id, $required = true): ?BasketItemInterface
    {
        $basketItem = $this->basketRepository->getBasketItem($this->getBuyer(), $this->getGood($id));
        if (empty($basketItem) && $required) {
            abort(404, 'Товар в корзине не найден');
        }
        return $basketItem;
    }

    /**
     * Возвращает текущего пользователя
     *
     * @param bool $required
     *
     * @return BuyerInterface
     */
    protected function getBuyer($required = true): ?BuyerInterface
    {
        $buyer = $this->buyerRepository->getCurrent();
        if (!empty($buyer)) {
            return $buyer;
        }
        if ($required) {
            return $this->buyerService->add();
        }
        return null;
    }

    /**
     * Возвращает товар
     *
     * @param int $id
     *
     * @return GoodInterface|null
     */
    protected function getGood(int $id)
    {
        $good = $this->goodRepository->getEntityForBasket($id);
        if (empty($good)) {
            abort(404, 'Товар не найден');
        }
        return $good;
    }

    /**
     * Список товаров в корзине
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $items = [];
        $totalSum = 0;
        $buyer = $this->getBuyer(false);
        if (!empty($buyer)) {
            $basketItems = $this->basketRepository->getAllByBuyer($buyer);
            foreach ($basketItems as $basketItem) {
                $good = $basketItem->getGood();
                if (empty($good)) {
                    $this->basketService->remove($basketItem);
                    continue;
                }
                if ($basketItem->getPrice() != $good->getPrice()) {
                    $this->basketService->updatePrice($basketItem, $good->getPrice());
                }
                $items[] = [
                    'id' => $good->getId(),
                    'name' => $good->getName(),
                    'url' => $good->getUrl(),
                    'thumbnail' => $good->getThumbnail(),
                    'quantity' => $basketItem->getQuantity(),
                    'price' => $basketItem->getPrice(),
                    'sum' => $basketItem->getSum(),
                    'formatted_price' => PriceHelper::format($basketItem->getPrice()),
                    'formatted_sum' => PriceHelper::format($basketItem->getSum()),
                ];
                $totalSum += $basketItem->getSum();
            }
        }
        return response()->json([
            'items' => $items,
            'count' => count($items),
            'total_sum' => $totalSum,
            'formatted_total_sum' => PriceHelper::format($totalSum),
        ]);
    }

    /**
     * Обновление товара
     *
     * @param int           $id
     * @param BasketRequest $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function update(int $id, BasketRequest $request): JsonResponse
    {
        $this->basketService->updateQuantity($this->getBasketItem($id), $request->get('quantity'));
        return $this->index();
    }

    /**
     * Удаление товара из корзины
     *
     * @param int $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function remove(int $id): JsonResponse
    {
        $this->basketService->remove($this->getBasketItem($id));
        return $this->index();
    }
}
