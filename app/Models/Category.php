<?php

namespace App\Models;

use App\Services\Shop\Interfaces\Entities\CategoryInterface;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @package App\Services\Shop\Entities
 * @property int     id
 * @property string  name
 * @property string  slug
 * @property integer sort
 * @property integer goods_count
 * @property string  created_at
 * @property string  updated_at
 */
class Category extends BaseModel implements CategoryInterface
{
    use SoftDeletes, Sluggable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'shop_categories';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['name', 'sort'];

    /**
     * @return HasMany
     */
    public function goods()
    {
        return $this->hasMany(Good::class, 'category_id', 'id');
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return url('shop/' . $this->getSlug());
    }

    /**
     * @inheritDoc
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @inheritDoc
     */
    public function getGoodsCount(): int
    {
        return $this->goods_count;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updated_at;
    }
}
