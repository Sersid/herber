<?php

namespace App\Services\Shop\Interfaces\Entities;

interface CategoryInterface
{
    /**
     * Id
     * @return integer
     */
    public function getId(): int;

    /**
     * Название категории
     * @return string
     */
    public function getName(): string;

    /**
     * Slug
     * @return string
     */
    public function getSlug(): string;

    /**
     * Url категории
     * @return string
     */
    public function getUrl(): string;

    /**
     * Кол-во товаров в категории
     * @return int
     */
    public function getGoodsCount(): int;

    /**
     * Дата создания
     * @return string
     */
    public function getCreatedAt(): ?string;

    /**
     * Дата обновления
     * @return string
     */
    public function getUpdatedAt(): ?string;
}
