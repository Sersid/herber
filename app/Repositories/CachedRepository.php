<?php

namespace App\Repositories;

use Cache;
use Closure;

abstract class CachedRepository
{
    /**
     * Текущая страница постраничной навигации
     * @return int
     */
    protected function getPage(): int
    {
        return (int)request()->get('page', 1);
    }

    /**
     * @param string  $key
     * @param Closure $fn
     *
     * @return mixed
     */
    protected function getCache(string $key, Closure $fn)
    {
        return Cache::tags($this->tag())
            ->remember($key, $this->durations(), $fn);
    }

    /**
     * @param string  $key
     * @param Closure $fn
     *
     * @return mixed
     */
    protected function getCachePaginate(string $key, Closure $fn)
    {
        return $this->getCache($key . '-' . $this->getPage(), $fn);
    }

    /**
     * Тэг
     * @return string
     */
    abstract protected function tag(): string;

    /**
     * Время жизни
     * @return int
     */
    abstract protected function durations(): int;
}
