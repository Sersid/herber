<?php

namespace App\Services\Shop\Interfaces\Entities;

interface ImageInterface
{
    /**
     * Id
     * @return int
     */
    public function getId(): int;

    /**
     * Оригинальное название изображения
     * @return string
     */
    public function getName(): string;

    /**
     * Расширение файла
     * @return string
     */
    public function getExt(): string;

    /**
     * Путь к маленькому изображению
     * @return string
     */
    public function getSmall(): string;

    /**
     * Путь к среднему изображению
     * @return string
     */
    public function getMedium(): string;

    /**
     * Путь к большому изображению
     * @return string
     */
    public function getLarge(): string;

    /**
     * Путь к директории с файлами относительно public/
     * @return string
     */
    public function getDir(): string;

    /**
     * Абсолютный путь к файлу
     * @return string
     */
    public function getDirPath(): string;

    /**
     * Путь к маленькому изображению
     * @return string
     */
    public function getSmallPath(): string;

    /**
     * Путь к среднему изображению
     * @return string
     */
    public function getMediumPath(): string;

    /**
     * Путь к большому изображению
     * @return string
     */
    public function getLargePath(): string;
}
