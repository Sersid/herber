<!-- Breadcrumbs -->
<section class="breadcrumbs-custom-inset">
    <div class="breadcrumbs-custom context-dark bg-overlay-39">
        <div class="container">
            <h2 class="breadcrumbs-custom-title">{{ $title }}</h2>
            <ul class="breadcrumbs-custom-path">
                <li><a href="/">Главная</a></li>
                @foreach($items as $item)
                    <li><a href="{{ $item['url'] }}">{{ $item['name'] }}</a></li>
                @endforeach
                <li class="active">{{ $title }}</li>
            </ul>
        </div>
        <div class="box-position" style="background-image: url({{ asset('images/bg-breadcrumbs.jpg') }});"></div>
    </div>
</section>
