<?php

namespace App\Services\Shop\Interfaces\Repositories;

use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\ImageInterface;
use Illuminate\Support\Collection;

interface ImageRepositoryInterface
{
    /**
     * Изображение по id
     * @param int $id
     *
     * @return ImageInterface|null
     */
    public function getEntity(int $id): ?ImageInterface;

    /**
     * Список изображений для формы товара
     * @param int|null $goodId
     *
     * @return Collection|ImageInterface[]
     */
    public function getForForm(?int $goodId): Collection;

    /**
     * Список изображений по товару
     * @param GoodInterface $good
     *
     * @return Collection|ImageInterface[]
     */
    public function getByGood(GoodInterface $good): Collection;
}
