<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class ShopCategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Фрукты', 'sort' => 10]);
        Category::create(['name' => 'Овощи', 'sort' => 20]);
        Category::create(['name' => 'Консервация', 'sort' => 30]);
        Category::create(['name' => 'Разное', 'sort' => 500]);
    }
}
