<?php

namespace App\Services\Shop;

use App\Models;
use App\Services\Shop\Images\ImageDisk;
use App\Services\Shop\Images\Strategies\LargeImageStrategy;
use App\Services\Shop\Images\Strategies\MediumImageStrategy;
use App\Services\Shop\Images\Strategies\SmallImageStrategy;
use App\Services\Shop\Interfaces\Entities\GoodInterface;
use App\Services\Shop\Interfaces\Entities\ImageInterface;
use App\Services\Shop\Interfaces\ImageServiceInterface;
use App\Services\Shop\Interfaces\Repositories\ImageRepositoryInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageService implements ImageServiceInterface
{
    /**
     * @return string
     */
    private function getRandomName(): string
    {
        return strtolower(Str::random(15));
    }

    /**
     * @inheritDoc
     */
    public function upload(UploadedFile $image, ?int $good_id): ImageInterface
    {
        $data = [
            'good_id' => $good_id,
            'name' => $image->getClientOriginalName(),
        ];
        $imageCreator = new ImageDisk($this->getRandomName(), $image->getClientOriginalExtension());
        File::ensureDirectoryExists($imageCreator->getDirPath(), 0777);
        $strategies = [
            'sm' => SmallImageStrategy::class,
            'md' => MediumImageStrategy::class,
            'lg' => LargeImageStrategy::class,
        ];
        foreach ($strategies as $attr => $strategy) {
            $imageCreator->setStrategy(new $strategy);
            $data[$attr] = $imageCreator->getUrl();
            Image::make($image)
                ->resize(
                    $imageCreator->getWidth(),
                    $imageCreator->getHeight(),
                    function ($constraint) {
                        $constraint->aspectRatio();
                    })
                ->save($imageCreator->getPath());
        }
        return Models\Image::create($data);
    }

    /**
     * @inheritDoc
     */
    public function remove(ImageInterface $image)
    {
        File::deleteDirectory($image->getDirPath());
        Models\Image::find($image->getId())
            ->delete();
    }

    /**
     * @inheritDoc
     */
    public function updateGoodForEmpty(GoodInterface $good)
    {
        Models\Image::where('good_id', null)
            ->update(['good_id' => $good->getId()]);
    }

    /**
     * @inheritDoc
     */
    public function copy(GoodInterface $fromGood, GoodInterface $toGood)
    {
        /** @var ImageRepositoryInterface $repository */
        $repository = app(ImageRepositoryInterface::class);
        $images = $repository->getByGood($fromGood);
        foreach ($images as $image) {
            $data = [
                'good_id' => $toGood->getId(),
                'name' => $image->getName(),
            ];
            $imageCreator = new ImageDisk($this->getRandomName(), $image->getExt());
            File::ensureDirectoryExists($imageCreator->getDirPath(), 0777);

            $imageCreator->setStrategy(new SmallImageStrategy);
            File::copy($image->getSmallPath(), $imageCreator->getPath());
            $data['sm'] = $imageCreator->getUrl();

            $imageCreator->setStrategy(new MediumImageStrategy);
            File::copy($image->getMediumPath(), $imageCreator->getPath());
            $data['md'] = $imageCreator->getUrl();

            $imageCreator->setStrategy(new LargeImageStrategy);
            File::copy($image->getLargePath(), $imageCreator->getPath());
            $data['lg'] = $imageCreator->getUrl();

            Models\Image::create($data);
        }
    }
}
