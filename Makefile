install: perm docker-install composer-create composer-clearcache perm chmod fresh-data
docker-install:
	docker-compose up --build -d
docker-up:
	docker-compose up -d
docker-down:
	docker-compose down
docker-restart: docker-down docker-up
bash:
	docker-compose exec php-fpm bash
perm:
	sudo chown ${USER}:${USER} ./* -R
chmod:
	docker-compose exec php-fpm chmod 777 -R /var/www/storage/
fresh-data:
	docker-compose exec php-fpm php artisan migrate:fresh --seed
composer-create:
	docker-compose exec php-fpm composer create-project
composer-clearcache:
	docker-compose exec php-fpm composer clearcache
clearcache:
	docker-compose exec php-fpm php artisan cache:clear
