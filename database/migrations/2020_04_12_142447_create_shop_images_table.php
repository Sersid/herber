<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopImagesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('shop_images',
            function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->softDeletes();
                $table->unsignedBigInteger('good_id')
                    ->nullable()
                    ->index();
                $table->string('name');
                $table->string('sm');
                $table->string('md');
                $table->string('lg');
            });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_images');
    }
}
