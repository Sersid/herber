@extends('template')

@section('content')
    @include('widgets.breadcrumbs', [
        'items' => [],
        'title' => 'Каталог продукции',
    ])
    <!-- Shop-->
    <section class="section section-xl bg-default">
        <div class="container">
            <div class="row row-90 justify-content-center">
                <div class="col-lg-8 col-xl-9">
                    @if($paginate->isNotEmpty())
                        <div class="product-top-panel group-lg">
                            <div class="product-top-panel-title">Показаны {{ $paginate->firstItem() }}–{{ $paginate->lastItem() }} из {{ $paginate->total() }}</div>
                            <div class="product-top-panel-select">
                                <!--Select 2-->
                                <select class="form-input select-filter" data-minimum-results-for-search="Infinity" data-constraints="@Required">
                                    <option value="1">Сортировать по цене</option>
                                    <option value="2">От меньшего к большему</option>
                                    <option value="3">От большего к меньшему</option>
                                </select>
                            </div>
                        </div>
                        <div class="row row-lg row-40">
                            @foreach($paginate->items() as $good)
                                <div class="col-sm-6 col-md-4">
                                    @include('widgets.product', ['good' => $good])
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h4>Товары не найдены</h4>
                    @endif
                    <div class="pagination-wrap">
                    {{ $paginate->links() }}
                    </div>
                </div>
                <div class="col-sm-10 col-md-12 col-lg-4 col-xl-3">
                    <!-- RD Search Form-->
                    <form class="form-search rd-search form-product-search" action="search-results.html" method="GET">
                        <div class="form-wrap">
                            <label class="form-label" for="search-form">Поиск..</label>
                            <input class="form-input" id="search-form" type="text" name="s" autocomplete="off">
                            <button class="button-search fl-bigmug-line-search74" type="submit"></button>
                        </div>
                    </form>
                    <div class="row row-lg row-50 product-sidebar">
                        <div class="col-md-6 col-lg-12">
                            <h5>Категории товаров</h5>
                            <ul class="list list-shop-filter">
                                @foreach($categories as $category)
                                    <li>
                                        <label class="checkbox-inline">
                                            <input name="input-group-radio" value="{{ $category->getSlug() }}" type="checkbox">{{ $category->getName() }}
                                        </label><span class="list-shop-filter-number">{{ $category->getGoodsCount() }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Footer-->
@endsection
